import pandas as pd
import sys
import re
from pathlib import Path
from os import listdir
from os.path import isfile, join
import rdflib
from rdflib import Graph, RDF, RDFS, Literal, OWL, URIRef, Namespace
import compute_equivalencies
import oxrdflib
import logging

def readGraph(filename):
    graph = Graph(store="Oxigraph")
    graph.parse(filename, format="turtle")

    return graph, list(graph.subjects())


def exportGraphAsTurtle(graph, path, filename):
    Path(path).mkdir(parents=True, exist_ok=True)
    file = open(path + "/" + filename, "w")
    file.write(graph.serialize(format="turtle"))
    file.close()

def scanFiles(path):
    filepaths = []
    try:
        filepaths = listdir(path)
        filepaths.sort()
    except:
        print("The filepath could not be scanned, stopping")
        exit
    print("Files scanned: "+ str(filepaths))

    # now also deassemble them into objects
    files = []
    for filepath in filepaths:
        match = re.search(r"sphn_([a-zA-Z0-9-]+)_([0-9]+)-?([0-9]+)?_?(versioned|sameConcepts|equivalencesToCurrent)?.ttl", filepath)
        ontologyfile = {}
        ontologyfile["name"] = (match.group(1))
        ontologyfile["version"] = (match.group(2))
        try:
            ontologyfile["type"] = (match.group(4))
        except:
            ontologyfile["type"] = None
        if ontologyfile["type"] == "sameConcepts":
            # if we have a sameConcepts file, then we also extract the second version where it is compared to
            ontologyfile["version2"] = (match.group(3))
        ontologyfile["filename"] = path+"/"+filepath
        files.append(ontologyfile)

    print("These are the files deassembled: " + str(files))
    return filepaths, files


def assureCorrectOntologyHeader(historizedGraph):
    # get the list of the version IRIs, and the rdfs:comment
    # store the newest element
    ontologyiri = list(historizedGraph.subjects(predicate=RDF.type, object=OWL.Ontology))[0]
    print("removing artefacts of versioning to the ontology iri: " + str(ontologyiri))
    comments = list(historizedGraph.objects(subject=ontologyiri, predicate=RDFS.comment))
    newestComment = max(comments)
    versioniris = list(historizedGraph.objects(subject=ontologyiri, predicate=OWL.versionIRI))
    newestVersionIris= max(versioniris)

    historizedGraph.remove((ontologyiri, RDFS.comment, None))
    historizedGraph.remove((ontologyiri, OWL.versionIRI, None))
    historizedGraph.add((ontologyiri, RDFS.comment, newestComment))
    historizedGraph.add((ontologyiri, OWL.versionIRI, newestVersionIris))

    cleanUpList = [URIRef("https://biomedit.ch/rdf/sphn-resource/icd-10-gm/BFARM"), URIRef("https://biomedit.ch/rdf/sphn-resource/icd-10-gm/BFS"), URIRef("https://biomedit.ch/rdf/sphn-resource/icd-10-gm/SPHN_DCC")]
    for item in cleanUpList:
        removeOlderPavVersionOfIRIIfExists(historizedGraph, item)
    pass


def removeOlderPavVersionOfIRIIfExists(historizedGraph, iri):
    pavVersion = URIRef("http://purl.org/pav/version")
    iriVersions = list(historizedGraph.objects(subject=iri, predicate=pavVersion))
    if len(iriVersions) > 0:
        newestIRIversion = max(iriVersions)
        historizedGraph.remove((iri, pavVersion, None))
        historizedGraph.add((iri, pavVersion, Literal(newestIRIversion)))
    else:
        print("There were no versions found for " + str(iri))
    pass





def addShortcutsForValidation(historizedGraph, equivalentGraph):
    # a) get a list of all distinct classes.
    # b) make a dictionary structure with the original code as key and all IRIs as their values
    # c) check whether the key is existing in check whether we have a path through the versions. 
    #    c1) For every class that has a path from the current, add hasMeaningValidityInCurrent "True",
    #    c2) if not add hasMeaningValidityInCurrent "False" and add the highest year to the unversioned
    subjects = list(historizedGraph.subjects())
    codeVersionMap = {}                     # versioned codes to check
    codeMeaningChangeInHistorization = {}   # unversioned codes to write the highest meaning change
    ontologyiri = str(list(historizedGraph.subjects(predicate=RDF.type, object=OWL.Ontology))[0])
    if ontologyiri == "https://biomedit.ch/rdf/sphn-resource/atc/" :
        ontologyName = "ATC"
    elif ontologyiri == "https://biomedit.ch/rdf/sphn-resource/icd-10-gm/":
        ontologyName = "ICD-10-GM"
    elif ontologyiri == "https://biomedit.ch/rdf/sphn-resource/chop/" :
        ontologyName = "CHOP"
    
    unversionedPrefix, versionedPrefixHeader, versionedPrefixTrailer, unversionedRootNodes = compute_equivalencies.getPrefix(ontologyName)

    for subject in subjects:
        # strip away the prefix and keep only the code, then fill it. 
            strSubject = str(subject)
            # check if the code is versioned or not:
            versioned = False
            codeVersioned = None
            yearVersioned = None
            try :
                codeVersioned = strSubject.split(versionedPrefixHeader, 1)[1].split(versionedPrefixTrailer, 1)[1]
                yearVersioned = strSubject.split(versionedPrefixHeader, 1)[1].split(versionedPrefixTrailer, 1)[0]
            except:
                pass
            if codeVersioned is not None and yearVersioned is not None:
                versioned = True
            if versioned :
                #print(strSubject)
                #print(versioned)
                code = strSubject.split(versionedPrefixHeader, 1)[1].split(versionedPrefixTrailer, 1)[1]
                if code in codeVersionMap.keys():
                    versions = codeVersionMap[code]
                    versions.append(subject)
                    codeVersionMap[code] = versions
                else :
                    codeVersionMap[code]= [strSubject]
            else:
                try :
                    code = strSubject.split(unversionedPrefix, 1)[1]
                    codeMeaningChangeInHistorization[code] = 0
                except :
                    print("not able to parse this in the meaningValiditiy " + str(strSubject))
    
    # c) check for every instance whether it goes through all the versions. (do this first for all the unversioned. )
    # we can achieve this by checking every iri, and see whether with a query we can reacht he unverioned version.
    # e.g. we have the 2016 Code of ATC C07FB02 we will not have any path to the current.
    for code in codeVersionMap.keys():
        for version in codeVersionMap[code]:
            # ask the historized graph, whether there is a path to the unversioned code via OWL.equivalentClass.
            unversionedCode = URIRef(unversionedPrefix + str(code))
            logging.basicConfig(level=logging.ERROR)
            res = equivalentGraph.query("""
                                  PREFIX owl: <http://www.w3.org/2002/07/owl#>
                                  SELECT (COUNT(*) as ?cnt)
                                  WHERE {
                                    <"""+URIRef(version)+"""> (owl:equivalentClass|^owl:equivalentClass)+ <"""+unversionedCode+"""> .
                                  } 
                                  """)
            logging.basicConfig(level=logging.DEBUG)


            hasPathToCurrent = int(str(list(res)[0][0].toPython()))
            if hasPathToCurrent > 0 :
                # add meaning Validity in Current
                historizedGraph.add((URIRef(version), URIRef("https://biomedit.ch/rdf/sphn-schema/sphn#hasMeaningValidityInCurrent"), Literal(True)))

            else :
                # add false Meaning Validity in current and add the Meaning Change in Historization to the unversioned one.
                historizedGraph.add((URIRef(version), URIRef("https://biomedit.ch/rdf/sphn-schema/sphn#hasMeaningValidityInCurrent"), Literal(False)))
                year = int(strSubject.split(versionedPrefixHeader, 1)[1].split(versionedPrefixTrailer, 1)[0])
                # as it could be that the code was deleted. If this is the case we cannot find it.
                if code in codeMeaningChangeInHistorization.keys():
                    if codeMeaningChangeInHistorization[code] < year :
                        codeMeaningChangeInHistorization[code] = year


    # now go through the codeMeaningChangeInHistorization and assign the unversioned codes the corresponding hasMeaningChangeInHistorization with the year where it is first new.
    for code in codeMeaningChangeInHistorization.keys():
        if int(codeMeaningChangeInHistorization[code]) > 0:
            unversionedCode = URIRef(unversionedPrefix + str(code))
            historizedGraph.add((unversionedCode, URIRef("https://biomedit.ch/rdf/sphn-schema/sphn#hasMeaningChangeInHistorization"), Literal(int(codeMeaningChangeInHistorization[code]))))

    pass



if __name__ == '__main__':
    # INPUT1 : we get a folder here (e.g. datastore/ATC)

    # In this path we can assume that there exist the following subdirectories with certain file namings:
    # datastore/ATC
    #    /equivalencesToCurrent --> e.g. sphn_atc_2016_equivalencesToCurrent.ttl
    #    /sameConcepts --> e.g. sphn_atc_2016-2017_sameConcepts.ttl
    #    /unversioned --> e.g. sphn_atc_2016-1.ttl
    #    /versioned --> e.g. sphn_atc_2016-1_versioned.ttl

    # the task of this script is to combine the data in these files to a release file.
    # The following procedure is used for generating version X for every X (e.g. year) we find in the files
    # 1) add all versioned graphs from /versioned that are <X and X
    # 2) add all sameConcepts graphs from /sameConcepts that are comparing to a version <X and X
    # 3) add the version for X from /equivalencesToCurrent
    # 4) add the version for X from /unversioned 
    
    # 5) add technical information about validity
    #  a) Add to the current unversioned code, when there has been a meaning change via: hasMeaningChangeInHistorization with the year as integer
    #  b) Add to each versioned code, whether it is valid according to the current unversioned version via: hasMeaningValidityInCurrent as a boolean
    
    ontology_root_path=sys.argv[1]
    versioned_file_path = ontology_root_path+"/versioned"
    unversioned_file_path = ontology_root_path+"/unversioned"
    sameConcepts_file_path = ontology_root_path+"/sameConcepts"
    equivalencesToCurrent_file_path = ontology_root_path+"/equivalencesToCurrent"


    # scan the folders to get the filenames etc:
    versioned_filepaths, versioned_files=scanFiles(versioned_file_path)
    unversioned_filepaths, unversioned_files=scanFiles(unversioned_file_path)
    sameConcepts_filepaths, sameConcepts_files=scanFiles(sameConcepts_file_path)
    equivalencesToCurrentfilepaths, equivalencesToCurrent_files=scanFiles(equivalencesToCurrent_file_path)

    oldestRelease = versioned_files[0]["version"]

    # 0: get a list of the possible years from the versioned
    for releaseX in versioned_files:
        print("Working on : " + str(releaseX))
        historizedGraph = Graph(store="Oxigraph")
        equivalentGraph = Graph(store="Oxigraph")
        # add all equal and older graphs from the /versioned
        print("1 : add all versioned graphs from /versioned that are <=" + str(releaseX))
        versionedItemsToAdd = [item for item in versioned_files if item["version"]<=releaseX["version"]]
        for item in versionedItemsToAdd:
            print("adding file : "+str(item["filename"]))
            graphToAdd, subjects = readGraph(item["filename"])
            historizedGraph += graphToAdd
        print("2 : add all sameConcepts graphs from /sameConcepts that are comparing to a version <=" + str(releaseX))
        versionedComparedItemsToAdd = [item for item in sameConcepts_files if item["version2"]<=releaseX["version"]]
        for item in versionedComparedItemsToAdd:
            print("adding file : "+str(item["filename"]))
            graphToAdd, subjects = readGraph(item["filename"])
            equivalentGraph += graphToAdd
            historizedGraph += graphToAdd
        print("3 : add the version for " + str(releaseX) + " from /equivalencesToCurrent")
        equivalencesToCurrentItemsToAdd = [item for item in equivalencesToCurrent_files if item["version"]==releaseX["version"]]
        if len(equivalencesToCurrentItemsToAdd) == 1:
            print("adding file : "+str(equivalencesToCurrentItemsToAdd[0]["filename"]))
            graphToAdd, subjects = readGraph(equivalencesToCurrentItemsToAdd[0]["filename"])
            equivalentGraph += graphToAdd
            historizedGraph += graphToAdd
        else:
            print("Was finding " + len(equivalencesToCurrentItemsToAdd) + " but should be exactly one. exiting")
            SystemExit(1)
        print("4 : add the version for " + str(releaseX) + " from /unversioned")
        unversionedItemsToAdd = [item for item in unversioned_files if item["version"]==releaseX["version"]]
        if len(unversionedItemsToAdd) == 1:
            print("adding file : "+str(unversionedItemsToAdd[0]["filename"]))
            graphToAdd, subjects = readGraph(unversionedItemsToAdd[0]["filename"])
            historizedGraph += graphToAdd
        else:
            print("Was finding " + len(unversionedItemsToAdd) + " but should be exactly one. exiting")
            SystemExit(1)

        assureCorrectOntologyHeader(historizedGraph)

        print("5 : Adding the technical information about the validity of the concepts")
        addShortcutsForValidation(historizedGraph, equivalentGraph)        

        # output is ontology_root_path+"/historized"
        exportGraphAsTurtle(historizedGraph, ontology_root_path+"/historized", "sphn_"+str(releaseX["name"])+"_"+str(releaseX["version"])+"-"+str(oldestRelease)+"-1.ttl")

