import pandas as pd
import sys
import re
from pathlib import Path
from os import listdir
from os.path import isfile, join
import rdflib
from rdflib import Graph, RDF, RDFS, Literal, OWL, URIRef, Namespace


PREFIX_MAP = { "ATC" : {"unversioned": "https://www.whocc.no/atc_ddd_index/?code=","versioned_header":  "https://www.whocc.no/atc_ddd_index/?year=", "versioned_trailer": "&code="},
         "ICD-10-GM" : {"unversioned": "https://biomedit.ch/rdf/sphn-resource/icd-10-gm/","versioned_header":  "https://biomedit.ch/rdf/sphn-resource/icd-10-gm/", "versioned_trailer": "/", "unversioned_ontology_roots": ["https://biomedit.ch/rdf/sphn-resource/icd-10-gm/", "https://biomedit.ch/rdf/sphn-resource/icd-10-gm/ICD-10-GM", "https://biomedit.ch/rdf/sphn-resource/icd-10-gm/BFS", "https://biomedit.ch/rdf/sphn-resource/icd-10-gm/BFARM" , "https://biomedit.ch/rdf/sphn-resource/icd-10-gm/SPHN_DCC", "https://www.bfs.admin.ch/", "https://www.bfarm.de/", "https://sphn.ch/network/data-coordination-center/"]},
         "CHOP" : {"unversioned": "https://biomedit.ch/rdf/sphn-resource/chop/","versioned_header":  "https://biomedit.ch/rdf/sphn-resource/chop/", "versioned_trailer": "/", "unversioned_ontology_roots": ["https://biomedit.ch/rdf/sphn-resource/chop/", "https://biomedit.ch/rdf/sphn-resource/chop/CHOP"]}}

BLACKLISTED_NODES= {"https://biomedit.ch/rdf/sphn-resource/icd-10-gm/": ["BFS", "DCC", "BFARM"]}



def scanFiles(path):
    filepaths = []
    try:
        filepaths = listdir(path)
        filepaths.sort()
    except:
        print("The filepath could not be scanned, stopping")
        exit
    print("Files scanned: "+ str(filepaths))

    # now also deassemble them into objects
    files = []
    for filepath in filepaths:
        match = re.search(r"sphn_([a-zA-Z0-9-]+)_([0-9]+)(-[0-9]+)?(_versioned)?.ttl", filepath)
        ontologyfile = {}
        ontologyfile["name"] = (match.group(1))
        ontologyfile["version"] =(match.group(2))
        ontologyfile["filename"] = path+"/"+filepath
        files.append(ontologyfile)

    print("These are the files deassembled: " + str(files))
    return filepaths, files


def conceptsUnchanged(concept, graph1, graph2, preprocess=True):
    conceptinGraph1 = (concept, None, None) in graph1
    conceptinGraph2 = (concept, None, None) in graph2

    # Do multilang and multilabel here:
    # first get all labels, then make a dictionary and compare the dictionaries
    labels1dict = {}
    labels2dict = {}
    try: 
        labels1 = list(graph1.objects(concept, RDFS.label))
        for label in labels1:
            lang = label.language if label.language is not None else ""
            if preprocess:
                label = preprocess_label(label)
            labels1dict[label.language] = label
    except:
        pass
    try:
        labels2 = list(graph2.objects(concept, RDFS.label))
        for label in labels2:
            lang = label.language if label.language is not None else ""
            if preprocess:
                label = preprocess_label(label)
            labels2dict[label.language] = label
    except:
        pass
    return (conceptinGraph1 and conceptinGraph2 and labels1dict==labels2dict)

def preprocess_label(label):
    """
    Pre-process and normalize a given label.
    """
    normalized_label = label
    normalized_label = normalized_label.lower()
    normalized_label = normalized_label.strip()
    if '<o>' in normalized_label and '</o>' in normalized_label:
        normalized_label = normalized_label.replace('<o>', '')
        normalized_label = normalized_label.replace('</o>', '')
    if '<n>' in normalized_label and '</n>' in normalized_label:
        normalized_label = normalized_label.replace('<n>', '')
        normalized_label = normalized_label.replace('</n>', '')
    if '[' in normalized_label and ']' in normalized_label:
        normalized_label = normalized_label.replace('[', '(')
        normalized_label = normalized_label.replace(']', ')')
    return normalized_label

def parseFileInfo(fileInfo):
    return fileInfo["name"], fileInfo["version"], fileInfo["filename"]

def readGraph(filename):
    graph = Graph()
    graph.parse(filename, format="turtle")

    return graph, list(graph.subjects())



def checkEquivalentConceptsOnPair(pair):
    name1, version1, filename1 = parseFileInfo(pair[0])
    name2, version2, filename2 = parseFileInfo(pair[1])

    graph1, subjects1 = readGraph(filename1)
    graph2, subjects2 = readGraph(filename2)

    numSame = 0
    sameConcepts = []
    deprecatedInNewCount = 0
    deprecatedInNew = []
    
    # we would either check on all Instances (e.g. UCUM) or on all instances of a class. It is easier to just check all Subjects. 
    # get all distinct subjects from both graphs together. They have the same IRIs as they are unversioned
    print("Distinct subjects of Graph1: " + str(len(set(subjects1))))
    print("Distinct subjects of Graph2: " + str(len(set(subjects2))))
    all_subjects = list(set(subjects1 + subjects2))

    print("Unique Subjects shared in both graphs: " + str(len(all_subjects)))

    for subj in all_subjects:
        # for every subject, we check whether it is there
        try:
            ischanged = conceptsUnchanged(subj, graph1, graph2)
            if ischanged:
                numSame += 1
                sameConcepts.append(subj)
            else:
                deprecatedInNewCount += 1
                deprecatedInNew.append(subj)
        except:
            print("could work with: "+str(subj))


    print("Stayed the same concepts : "+str(numSame))
    print("Deprecated/Missing Meaning : "+str(deprecatedInNewCount))
    #print("Deprecated/Missing Meaning : "+str(deprecatedInNew))

    return sameConcepts, deprecatedInNew

def getPrefix(ontologyEntry):
    unversionedPrefix = PREFIX_MAP[ontologyEntry.upper()]["unversioned"]
    versionedPrefixHeader = PREFIX_MAP[ontologyEntry.upper()]["versioned_header"]
    versionedPrefixTrailer = PREFIX_MAP[ontologyEntry.upper()]["versioned_trailer"]
    unversionedRootNodes = PREFIX_MAP[ontologyEntry.upper()]["unversioned_ontology_roots"] if \
        ("unversioned_ontology_roots" in PREFIX_MAP[ontologyEntry.upper()]) else unversionedPrefix
    return unversionedPrefix, versionedPrefixHeader, versionedPrefixTrailer, unversionedRootNodes

def getPrefixPairs(pair):
    name1, version1, filename1 = parseFileInfo(pair[0])
    name2, version2, filename2 = parseFileInfo(pair[1])

    if (name1 != name2):
        print("Name of ontology 1 : " + str(name1) + ";Name of ontology 2 : " + str(name2))
        print("the two files that should be compared are from a different ontology, exiting")
        exit
    # find the information in the PREFIX_MAP by using the name
    unversionedPrefix, versionedPrefixHeader, versionedPrefixTrailer, unversionedRootNodes = getPrefix(name1)
    oldPrefix = versionedPrefixHeader + str(version1) + versionedPrefixTrailer
    newPrefix = versionedPrefixHeader + str(version2) + versionedPrefixTrailer

    return unversionedPrefix, oldPrefix, newPrefix, unversionedRootNodes

def addEquivalentConceptsBetweenGraphs(graph, listEquivalentURIs, unversionedPrefix, oldPrefix, newPrefix, unversionedRootNodes):
    # for every concept in the equivalentList we are adding the unversioned URI beeing equivalent to the oldPrefixed and the newPrefixed one
    # also apply the pre-inference for the owl:equivalentClass to rdfs:subClassOf
    print("adding equivalencies on unversioned prefix: " + unversionedPrefix)
    for unversioned_entity in listEquivalentURIs:
        # only do the equivalency for the real payload URIs, in some cases there will be URIs from other sources as prefix, we exclude them. e.g. Root entry nodes into the external Terminology
        if (str(unversioned_entity).startswith(unversionedPrefix) and str(unversioned_entity) not in unversionedRootNodes):
            oldPrefix_entity = URIRef(oldPrefix + str(unversioned_entity).split(unversionedPrefix)[1])
            newPrefix_entity = URIRef(newPrefix + str(unversioned_entity).split(unversionedPrefix)[1])
            if str(oldPrefix_entity) not in unversionedRootNodes and str(newPrefix_entity) not in unversionedRootNodes:
                if str(oldPrefix_entity) == "https://biomedit.ch/rdf/sphn-resource/chop/CHOP" or str(newPrefix_entity) == "https://biomedit.ch/rdf/sphn-resource/chop/CHOP" :
                    print("oldPrefix_entity : " + str(oldPrefix_entity))
                    print("newPrefix_entity : " + str(newPrefix_entity))

                #graph.add((unversioned_entity, OWL.equivalentClass, oldPrefix_entity))
                #graph.add((unversioned_entity, OWL.equivalentClass, newPrefix_entity))
                graph.add((oldPrefix_entity, OWL.equivalentClass, newPrefix_entity))

                # pre-inference
                #graph.add((unversioned_entity, RDFS.subClassOf, oldPrefix_entity))
                #graph.add((unversioned_entity, RDFS.subClassOf, newPrefix_entity))
                #graph.add((oldPrefix_entity, RDFS.subClassOf, unversioned_entity))
                #graph.add((newPrefix_entity, RDFS.subClassOf, unversioned_entity))
                graph.add((oldPrefix_entity, RDFS.subClassOf, newPrefix_entity))
                graph.add((newPrefix_entity, RDFS.subClassOf, oldPrefix_entity))
            else:
                print("skipping : " + str(unversioned_entity) + " in the equivalency calculation")
        else:
            print("skipping : " + str(unversioned_entity) + " in the equivalency calculation")
    return graph


def addMeaningVersionedToUnversioned(graph, listURIs, unversionedPrefix, versionedPrefix, unversionedRootNodes):
    for unversioned_entity in listURIs:
    # only do the equivalency for the real payload URIs, in some cases there will be URIs from other sources as prefix, we exclude them. e.g. Root entry nodes into the external Terminology
        if (str(unversioned_entity).startswith(unversionedPrefix) and str(unversioned_entity) not in unversionedRootNodes):
            versionedPrefix_entity = URIRef(versionedPrefix + str(unversioned_entity).split(unversionedPrefix)[1])
            if str(versionedPrefix_entity) not in unversionedRootNodes:
                if str(versionedPrefix_entity) == "https://biomedit.ch/rdf/sphn-resource/chop/CHOP" or str(unversioned_entity) == "https://biomedit.ch/rdf/sphn-resource/chop/CHOP" :
                    print("versionedPrefix_entity : " + str(versionedPrefix_entity))
                graph.add((unversioned_entity, OWL.equivalentClass, versionedPrefix_entity))

                # pre-inference
                graph.add((unversioned_entity, RDFS.subClassOf, versionedPrefix_entity))
                graph.add((versionedPrefix_entity, RDFS.subClassOf, unversioned_entity))

                # versioning shortcuts
                graph.add((versionedPrefix_entity, URIRef("https://biomedit.ch/rdf/sphn-schema/sphn#isCurrent"), Literal(True)))
            else :
                print("skipping : " + str(versionedPrefix_entity) + " in the equivalency calculation")
        else :
            print("skipping : " + str(unversioned_entity) + " in the equivalency calculation")
    return graph


def exportGraphAsTurtle(graph, path, filename):
    Path(path).mkdir(parents=True, exist_ok=True)
    file = open(path + "/" + filename, "w")
    file.write(graph.serialize(format="turtle"))
    file.close()


if __name__ == '__main__':
    # INPUT : we get a folder here (e.g. datastore/ATC)
    # in this we will find a subfolder "versioned" and "unversioned"
    # in both there are the same number of files, just one versioned and one unversioned one. 

    # (the unversioned files may be used for this) we will need to do pairwise comparison for every two versions that are adjacent:
    # 2016-2017, 2017-2018, 2018-2019
    # we compared where the concepts stayed the same, if so we add the
    # But keep in mind, that the output needs to use versioned IRIs

    # At the same time we could also produce the equivalencies as each file would be the newest current.


    # the whole function should also work if only a single file is provided. Then of course no concept stayed the same.
    # we always do a full run, as the content/order etc might change between loads. A conditional run if the input is completely unchanged could be done as a wrapper finally.


    # we would need to read the scheme for the versioned iris from the versioned files.
    ontology_root_path=sys.argv[1]
    versioned_file_path = ontology_root_path+"/versioned"
    unversioned_file_path = ontology_root_path+"/unversioned"

    # scan the folders to get the filenames etc:
    versioned_filepaths, versioned_files=scanFiles(versioned_file_path)
    unversioned_filepaths, unversioned_files=scanFiles(unversioned_file_path)

    # now we have sorted the files that need to be processed in ascending order
    # we so now do a pairwise comparison of the unversioned files. 
    print("compute pairwise comparison")
    for pair in zip(*[unversioned_files[i:] for i in range(2)]):
        print(str(pair))
        sameConcepts, deprecatedInNew = checkEquivalentConceptsOnPair(pair)

        unversionedPrefix, oldPrefix, newPrefix, unversionedRootNodes = getPrefixPairs(pair)
        print("prefixes:")
        print(unversionedPrefix)
        print(oldPrefix)
        print(newPrefix)
        # for the same concepts we insert the equivalency
        equivalentPairGraph = Graph()
        addEquivalentConceptsBetweenGraphs(equivalentPairGraph, sameConcepts, unversionedPrefix, oldPrefix, newPrefix, unversionedRootNodes)
        print("The equivalent graph has: "+ str(len(equivalentPairGraph)) + " triples")
        
        # writing out the comparisons
        exportGraphAsTurtle(equivalentPairGraph, ontology_root_path+"/sameConcepts", "sphn_"+str(pair[0]["name"])+"_"+ str(pair[0]["version"])+"-"+str(pair[1]["version"])+"_sameConcepts.ttl")

    print("compute the current meaning for each ontology individually")
    for ontology in unversioned_files:
        print(ontology)
        name, version, filename = parseFileInfo(ontology)
        graph, subjects = readGraph(filename)
        unversionedPrefix, versionedPrefixHeader, versionedPrefixTrailer, unversionedRootNodes = getPrefix(name)
        VersionedToNewestGraph = Graph()
        addMeaningVersionedToUnversioned(VersionedToNewestGraph, subjects, unversionedPrefix, versionedPrefixHeader+str(version)+versionedPrefixTrailer, unversionedRootNodes)
        exportGraphAsTurtle(VersionedToNewestGraph, ontology_root_path+"/equivalencesToCurrent", "sphn_"+str(name)+"_"+str(version)+"_equivalencesToCurrent.ttl")

