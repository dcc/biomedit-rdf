#!/bin/bash

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

echo "script directory is $SCRIPT_DIR"
echo "gathering terminology tests"
tests=$(find $SCRIPT_DIR/../tests -type f)

for testfile in $tests
do
    echo "running test from $testfile"
    query_content=$(cat $testfile | jq -sRr @uri)
    query_parameter="query=$query_content"
    echo "$query_parameter"
    res_complete="$(/bin/curl --get --silent --header 'Accept: text/csv' --data $query_parameter http://localhost:3030/terminologies/sparql)"
    echo "raw result: $res_complete"
    res=$(echo "$res_complete" | sed -n 2p | tr -d "\r")
    echo "converted result: $res"
    if [ "$res" == "true" ]
    then
        echo "PASSED : test from $testfile "
    else
        echo "FAILED : test from $testfile"
        echo "aborting"
        exit 1
    fi

done

