#!/bin/bash

# this is a script that runs as long as there is now response from a service
host=$1
port=$2

iter=0
max_iter=100
wait_time_s=10

while [ $iter -le $max_iter ]
do 
    echo "Current iteration : $iter"
    retval=$(curl -s -o /dev/null -w "%{http_code}" $host:$port)
    if [[ $retval == "200"* ]]
    then
        echo "Service is up and running, exiting"
        exit 0
    fi
    echo "not online, waiting to try again in $wait_time_s"
    sleep $wait_time_s
    iter=$((iter+1))
done 

echo "Max retries reached, exiting"
exit 1