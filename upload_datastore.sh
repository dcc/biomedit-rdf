#!/bin/bash
## upload_datastore.sh
## Downloads the latest release of the terminology service, removes all old content that is replaced with new content (leaves the content that is still valid untouched). The old bundle is moved to the archive. The new bundle is then uploaded again.



MC_COMMAND=./mc
SOURCEFOLDER=data
FOLDERCONTENT=$(ls $SOURCEFOLDER)
FORMATTEDDATE=$(date +"%Y-%m-%d_%H-%M-%S")


# configure
echo "Set minio alias to $MINIO_ENDPOINT"
eval "$MC_COMMAND alias set local $MINIO_ENDPOINT $MINIO_ACCESS_KEY $MINIO_SECRET_KEY"


# Finally we can upload the generated data
echo "Uploading current ontologies to local/datastore/"
eval "ls -lshR $SOURCEFOLDER"
eval "$MC_COMMAND cp --recursive ./$SOURCEFOLDER/ local/datastore/"
