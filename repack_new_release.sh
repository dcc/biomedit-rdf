#!/bin/bash
## repack_new_release.sh
## Downloads the latest release of the terminology service, removes all old content that is replaced with new content (leaves the content that is still valid untouched). The old bundle is moved to the archive. The new bundle is then uploaded again.


MC_COMMAND=./mc
SOURCEFOLDER=data
FOLDERCONTENT=$(ls $SOURCEFOLDER)
TARGETFOLDER=ontologies_temp
FORMATTEDDATE=$(date +"%Y-%m-%d_%H-%M-%S")
DEVOUT=$1

# abort if there are no new things to do
echo "SOURCE FOLDER ELEMENT SIZE IS: ${#FOLDERCONTENT[@]}"
echo "Delete Elements are: $DELETE_OUTPUT_FILES"

# configure
echo "Set minio alias to $MINIO_ENDPOINT"
eval "$MC_COMMAND alias set local $MINIO_ENDPOINT $MINIO_ACCESS_KEY $MINIO_SECRET_KEY"

# download
echo "Downloading current ontologies to $TARGETFOLDER"
eval "$MC_COMMAND cp -recursive local/ontologies/current/ ./$TARGETFOLDER"

# verify integrity
if [ "$VERIFYINTEGRITY" = "true" ]
then
    echo "Assure the data is also from the intended recipient and resolves correctly"
    eval "cat $GPG_PUB_KEY_CONTENT | gpg --batch --import"
    verified=$(gpg --verify ./$TARGETFOLDER/ontologies-*.gpg ./$TARGETFOLDER/ontologies-*.zip 2>&1 | grep 'Good signature from')
    echo "Content of verified value: $verified "
    if [[ "gpg: Good signature from \"DCC <dcc@sib.swiss>\" [unknown]" == "$verified" ]]
    then
    echo "Signature verified"
    else
        echo "Signature wrong : aborting"
        exit 1
    fi
else
    echo "Signature was not checked as VERIFYINTEGRITY was set to: $VERIFYINTEGRITY"
fi

# unpack
echo "unpacking the current ontologies"
eval "cd ./$TARGETFOLDER && unzip ontologies*.zip && rm ontologies*.zip && rm *.gpg && cd .."


#delete source folders in the target, this leaves the folders untouched, were nothing new is there.
echo "Applying deletes specified in the variable: DELETE_OUTPUT_FILES"
for val in $DELETE_OUTPUT_FILES
do
        echo "Removing if existing: ./$TARGETFOLDER/$val"
        eval "rm -rf ./$TARGETFOLDER/$val"
done

# copy over the new data
for val in $FOLDERCONTENT
do
    echo "Copying over existing: ./$SOURCEFOLDER/$val"
    eval "cp -rf ./$SOURCEFOLDER/$val ./$TARGETFOLDER"
done

# remove all old data, only keep the file with the highest number
eval "ls -ltr $TARGETFOLDER"
FOLDERCONTENTTARGET=$(ls $TARGETFOLDER)
for val in $FOLDERCONTENTTARGET
do
    echo "deleting all old files in: ./$TARGETFOLDER/$val . Listing the folder content before the cleanup and after"
    eval "ls -ltr $TARGETFOLDER/$val"
    eval "cd ./$TARGETFOLDER/$val && ls -1 | sort -r | tail -n +2 | xargs -r rm && cd ../.."
    eval "ls -ltr $TARGETFOLDER/$val"
done

echo "remove the sources.txt if exists"
eval "rm -f ./$TARGETFOLDER/sources.txt"

echo "listing the files in the TARGETFOLDER"
eval "ls -ltr ./$TARGETFOLDER"

# repack 
echo "repack the content of the ontology bundle for version $FORMATTEDDATE"
cd ./$TARGETFOLDER && zip -r ontologies-$FORMATTEDDATE.zip * && cd ..

# sign the data
echo "Signing the package"
eval "cat $GPG_PRIV_KEY_CONTENT | gpg --batch --import"
eval "echo $GPG_PRIV_KEY_PASS | gpg --batch --always-trust --yes --passphrase-fd 0 --pinentry-mode=loopback -s $(mktemp)"
eval "gpg --detach-sign --local-user dcc@sib.swiss -o ./$TARGETFOLDER/ontologies-$FORMATTEDDATE.gpg ./$TARGETFOLDER/ontologies-$FORMATTEDDATE.zip"

# if unset:
if [ -z ${1+x} ]
then

    # Then we can move the data from the sources/current to the sources/archive ==> ____ <<-- THIS IS DEPRECATED _____and the ontologies/current to the ontologies/archive 
    #echo "Apply the archival process on the current sources if Demomode is false. Current setting is: $DEMOMODE"
    #eval 'if [ "$DEMOMODE" = "false" ]; then $MC_COMMAND cp --recursive local/sources/current/ local/sources/archive && $MC_COMMAND rm --recursive --force local/sources/current/ ; fi'
    echo "Apply the archival process on the current ontology"
    eval "$MC_COMMAND cp --recursive local/ontologies/current/ local/ontologies/archive/ && $MC_COMMAND rm --recursive --force local/ontologies/current/"

    # Finally we can upload the generated data
    echo "Uploading current ontologies to local/ontologies/current/"
    eval "ls -lshR $TARGETFOLDER"
    eval "$MC_COMMAND cp ./$TARGETFOLDER/ontologies-$FORMATTEDDATE.zip local/ontologies/current/"
    echo "Uploading also the signature to local/ontologies/current/"
    eval "$MC_COMMAND cp ./$TARGETFOLDER/ontologies-$FORMATTEDDATE.gpg local/ontologies/current/"

# if DEVOUT is set:
else
    # for the devout we don't do any archival, nor do we upload anything to the current. We only overwrite the output in the dev-out

    # Finally we can upload the generated data
    echo "Uploading current ontologies to local/dev-output/current/"
    eval "ls -lshR $TARGETFOLDER"
    eval "$MC_COMMAND cp ./$TARGETFOLDER/ontologies-$FORMATTEDDATE.zip local/dev-output/current/"
    echo "Uploading also the signature to local/dev-output/current/"
    eval "$MC_COMMAND cp ./$TARGETFOLDER/ontologies-$FORMATTEDDATE.gpg local/dev-output/current/"

fi
