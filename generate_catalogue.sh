# !/bin/bash

# the plan is to download the new ontologies from the sources location. (done before that script -> we start with the f># then we need to make file, that lists for all ontologies the corresponding input paths.

# download the data is done via:
# ./mc cp --recursive local/sources/current/ sources/

# say hello
echo "generating the catalogue"

# clean potentially existing sources file
rm -f sources/sources.txt

# the next command needed a removal of the trailing slash on the sources for the mac. See whether it also runs on linux.
# list the data files in sources recursively
find ./sources -name \*.* -print >> sources.txt

# configure a python environment
pip install pandas

# start the python application to make the 
python generate_catalogue.py

# to get the path later for SNOMED it is:
# cat sources.txt | grep SNOMED | cut -d$'\t' -f1


