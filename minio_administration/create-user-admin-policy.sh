#!/bin/bash

# Syntax
# ------
#   ./create-user-admin-policy.sh
#

MINIO_ALIAS=sib
VERSION="2012-10-17" # version of the policy interpreter!

if [ "$#" -ne 0 ]; then
  echo "Illegal number of parameters"
  exit 1
fi

USER_ADMIN_POLICY=USER_ADMIN_POLICY

echo "Creating user admin policy for $MINIO_ALIAS"
echo USER_ADMIN_POLICY: $USER_ADMIN_POLICY


mkdir -p /apps/tmp/minio-policy/
cat << EOF > /apps/tmp/minio-policy/${USER_ADMIN_POLICY}.json
{
   "Version":"$VERSION",
   "Statement": [
    {
       "Sid": "AllowUserManagement",
       "Action": ["admin:CreateUser","admin:DeleteUser", "admin:ListUsers", "admin:EnableUser", "admin:DisableUser", "admin:GetUser", "admin:AddUserToGroup", "admin:RemoveUserFromGroup", "admin:GetGroup", "admin:ListGroups", "admin:GetPolicy", "admin:AttachUserOrGroupPolicy", "admin:ListUserPolicies"],
       "Effect": "Allow",
       "Resource": ["arn:aws:s3:::*"]
     }
   ]
}
EOF


mc admin user add ${MINIO_ALIAS} dummyuser dummypassword

mc admin group add ${MINIO_ALIAS} $USER_ADMIN_POLICY dummyuser

mc admin policy add ${MINIO_ALIAS} $USER_ADMIN_POLICY /apps/tmp/minio-policy/$USER_ADMIN_POLICY.json

mc admin policy set ${MINIO_ALIAS} $USER_ADMIN_POLICY group=$USER_ADMIN_POLICY

mc admin group remove ${MINIO_ALIAS} $USER_ADMIN_POLICY dummyuser

mc admin user remove ${MINIO_ALIAS} dummyuser
