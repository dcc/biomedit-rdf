#!/bin/bash

# Syntax
# ------
#   ./add-user-to-groups.sh user groups
#

MINIO_ALIAS=sib
ACCESSKEY=$1

if [ "$#" -lt 2 ]; then
  echo "Illegal number of parameters"
  exit 1
fi

for GROUP in "${@:2}"; do
  mc admin group add $MINIO_ALIAS $GROUP $ACCESSKEY
done
