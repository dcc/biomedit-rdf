#!/bin/bash

# Syntax
# ------
#   ./create-new-prefix.sh bucket-name prefix/sub-prefix
#

MINIO_ALIAS=sib
BUCKET_NAME=$1
PATHPREFIX=$2
VERSION="2012-10-17" # version of the policy interpreter!

if [ "$#" -ne 2 ]; then
  echo "Illegal number of parameters"
  exit 1
fi

PREFIX=${PATHPREFIX//\//_} 
RO_POLICY=${BUCKET_NAME}_${PREFIX}_RO
RW_POLICY=${BUCKET_NAME}_${PREFIX}_RW
RWD_POLICY=${BUCKET_NAME}_${PREFIX}_RWD

echo "Creating policies and groups for prefix $PATHPREFIX..."
echo RO_POLICY: $RO_POLICY
echo RW_POLICY: $RW_POLICY
echo RWD_POLICY: $RWD_POLICY

mkdir -p ./apps/tmp/minio-policy/
cat << EOF > ./apps/tmp/minio-policy/${RO_POLICY}.json
{
   "Version":"$VERSION",
   "Statement": [
    {
       "Sid": "AllowListBucketConsole",
       "Action": ["s3:ListAllMyBuckets","s3:GetBucketLocation"],
       "Effect": "Allow",
       "Resource": ["arn:aws:s3:::*"]
     },
    {
       "Sid": "AllowListofObjectsinRoot",
       "Action": ["s3:ListBucket"],
       "Effect": "Allow",
       "Resource": ["arn:aws:s3:::$BUCKET_NAME"]
     },
    {
        "Sid": "GetObject",
        "Action": ["s3:GetObject"],
        "Effect": "Allow",
        "Resource": ["arn:aws:s3:::$BUCKET_NAME/$PATHPREFIX/*","arn:aws:s3:::$BUCKET_NAME/$PATHPREFIX"]
     }
   ]
}
EOF

cat << EOF > ./apps/tmp/minio-policy/${RW_POLICY}.json
{
   "Version":"$VERSION",
   "Statement": [
    {
       "Sid": "AllowListBucketConsole",
       "Action": ["s3:ListAllMyBuckets","s3:GetBucketLocation"],
       "Effect": "Allow",
       "Resource": ["arn:aws:s3:::*"]
     },
    {
       "Sid": "AllowListofObjectsinRoot",
       "Action": ["s3:ListBucket"],
       "Effect": "Allow",
       "Resource": ["arn:aws:s3:::$BUCKET_NAME"]
     },
    {
       "Sid": "AllowListListBucketMultiUpload",
       "Action": ["s3:ListBucketMultipartUploads"],
       "Effect": "Allow",
       "Resource": ["arn:aws:s3:::$BUCKET_NAME"]
     },
    {
        "Sid": "GetObject",
        "Action": ["s3:GetObject"],
        "Effect": "Allow",
        "Resource": ["arn:aws:s3:::$BUCKET_NAME/$PATHPREFIX/*","arn:aws:s3:::$BUCKET_NAME/$PATHPREFIX"]
     },
    {
        "Sid": "PutObject",
        "Action": ["s3:PutObject"],
        "Effect": "Allow",
        "Resource": ["arn:aws:s3:::$BUCKET_NAME/$PATHPREFIX/*"]
     }
   ]
}
EOF

cat << EOF > ./apps/tmp/minio-policy/${RWD_POLICY}.json
{
   "Version":"$VERSION",
   "Statement": [
    {
       "Sid": "AllowListBucketConsole",
       "Action": ["s3:ListAllMyBuckets","s3:GetBucketLocation"],
       "Effect": "Allow",
       "Resource": ["arn:aws:s3:::*"]
     },
    {
       "Sid": "AllowListofObjectsinRoot",
       "Action": ["s3:ListBucket"],
       "Effect": "Allow",
       "Resource": ["arn:aws:s3:::$BUCKET_NAME"]
     },
    {
       "Sid": "AllowListBucketMultiUpload",
       "Action": ["s3:ListBucketMultipartUploads"],
       "Effect": "Allow",
       "Resource": ["arn:aws:s3:::$BUCKET_NAME"]
     },
    {
        "Sid": "GetObject",
        "Action": ["s3:GetObject"],
        "Effect": "Allow",
        "Resource": ["arn:aws:s3:::$BUCKET_NAME/$PATHPREFIX/*","arn:aws:s3:::$BUCKET_NAME/$PATHPREFIX"]
     },
    {
        "Sid": "PutObject",
        "Action": ["s3:PutObject"],
        "Effect": "Allow",
        "Resource": ["arn:aws:s3:::$BUCKET_NAME/$PATHPREFIX/*"]
     },
    {
        "Sid": "Delete",
        "Action": ["s3:DeleteObject"],
        "Effect": "Allow",
        "Resource": ["arn:aws:s3:::$BUCKET_NAME/$PATHPREFIX/*"]
     }
   ]
}
EOF

mc admin user add ${MINIO_ALIAS} dummyuser dummypassword

mc admin group add ${MINIO_ALIAS} $RO_POLICY dummyuser
mc admin group add ${MINIO_ALIAS} $RW_POLICY dummyuser
mc admin group add ${MINIO_ALIAS} $RWD_POLICY dummyuser

mc admin policy add ${MINIO_ALIAS} $RO_POLICY ./apps/tmp/minio-policy/$RO_POLICY.json
mc admin policy add ${MINIO_ALIAS} $RW_POLICY ./apps/tmp/minio-policy/$RW_POLICY.json
mc admin policy add ${MINIO_ALIAS} $RWD_POLICY ./apps/tmp/minio-policy/$RWD_POLICY.json

mc admin policy set ${MINIO_ALIAS} $RO_POLICY group=$RO_POLICY
mc admin policy set ${MINIO_ALIAS} $RW_POLICY group=$RW_POLICY
mc admin policy set ${MINIO_ALIAS} $RWD_POLICY group=$RWD_POLICY

mc admin group remove ${MINIO_ALIAS} $RO_POLICY dummyuser
mc admin group remove ${MINIO_ALIAS} $RW_POLICY dummyuser
mc admin group remove ${MINIO_ALIAS} $RWD_POLICY dummyuser
mc admin user remove ${MINIO_ALIAS} dummyuser
