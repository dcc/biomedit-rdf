#!/bin/bash

# Syntax
# ------
#   ./create-new-bucket.sh bucketname
#

MINIO_ALIAS=sib
BUCKET_NAME=$1

if [ "$#" -ne 1 ]; then
  echo "Illegal number of parameters"
  exit 1
fi

mc mb ${MINIO_ALIAS}/$BUCKET_NAME
mc version enable ${MINIO_ALIAS}/$BUCKET_NAME
