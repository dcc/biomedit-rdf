# /bin/bash

if [ -z "$(which curl)" ] ; then
     echo "curl is not installed! aborting"
	 exit 1
fi

if [ -z "$(which openssl)" ] ; then
     echo "openssl is not installed! aborting"
	 exit 1
fi

if [ -z "$(which jq)" ] ; then
     echo "jq is not installed! aborting"
	 exit 1
fi


bucket=$1
sourceprefix=$2
outfolder=$3

host=terminology-server.dcc.sib.swiss
s3_key='***'
s3_secret='***'

date=`date +"%a, %d %b %Y %H:%M:%S %z"`


download_individual_file () {
	resource="/$1"
	outfile=$2
	content_type="application/octet-stream"
	date=`date -R`
	_signature="GET\n\n${content_type}\n${date}\n${resource}"
	signature=`echo -en ${_signature} | openssl sha1 -hmac ${s3_secret} -binary | base64`
	echo "download from ${host} ${resource} to ${outfile}"
	curl \
			  -H "Host: $host" \
			  -H "Date: ${date}" \
			  -H "Content-Type: ${content_type}" \
			  -H "Authorization: AWS ${s3_key}:${signature}" \
			  -o ${outfile} \
			  --create-dirs \
			  https://${host}${resource}
}


authentication=$(curl "https://${host}/minio/webrpc" -H 'User-Agent: Mozilla' -H 'Content-Type: application/json' -H 'x-amz-date: ${date}' -H "Origin: https://{$host}" --data '{"id":1,"jsonrpc":"2.0","params":{"username":"'${s3_key}'","password":"'${s3_secret}'"},"method":"web.Login"}')


authentication_token=$(echo $authentication | jq -r '.result.token' )


file_list_raw=$(curl "https://${host}/minio/webrpc" -H "Authorization: Bearer $authentication_token" -H 'User-Agent: Mozilla' -H 'Content-Type: application/json' -H 'x-amz-date: ${date}' -H "Origin: https://{$host}" --data '{"id":1,"jsonrpc":"2.0","params":{"bucketName":"'${bucket}'","prefix":"'$sourceprefix'"},"method":"web.ListObjects"}')

file_list=$(echo $file_list_raw | jq -r '.result.objects[].name' )

for file in $file_list ; do
	echo "downloading "$file
	download_individual_file $bucket/$file $outfolder/$file
done

echo "done"

