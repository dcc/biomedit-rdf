#!/bin/bash

# Syntax
# ------
#   ./create-new-user.sh user userpw groups
#

MINIO_ALIAS=sib
ACCESSKEY=$1
SECRETKEY=$2

if [ "$#" -lt 2 ]; then
  echo "Illegal number of parameters"
  exit 1
fi

mc admin user add ${MINIO_ALIAS} $ACCESSKEY $SECRETKEY
for GROUP in "${@:3}"; do
  mc admin group add ${MINIO_ALIAS} $GROUP $ACCESSKEY
done
