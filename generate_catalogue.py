import pandas as pd
import os
import re

# functions

def parseFolderStructure(path):
    subpath_to_ontology = getOntologySubpath(path)
    ontology_name = subpath_to_ontology.split("/")[0]
    return ontology_name

def getOntologySubpath(path):
    return path.split("./sources/")[1]

def getOntologyFileNameAndType(path):
    return getOntologySubpath(path).split("/")[1], parseFolderStructure(path)

def outputNameFromVersion(path):
    outputName, version, year = outputNameAndVersionFromPath(path) 
    return outputName

def outputYearFromVersion(path):
    outputName, version, year = outputNameAndVersionFromPath(path) 
    return year


def outputVersionFromVersion(path):
    outputName, version, year = outputNameAndVersionFromPath(path) 
    return version


def outputNameAndVersionFromPath(path):
    # add also some versioning
    # CHOP : CHOP_2020_Multilang_CSV_FR.csv : chop_2020-1
    # ATC : atc_2021.xlsx : sphn_atc_2021
    # SNOMED : SnomedCT_InternationalRF2_PRODUCTION_20200731T120000Z.zip : snomed-ct-20200731
    print(path)
    ontology_name, ontology_type = getOntologyFileNameAndType(path)
    print(ontology_name)
    print(ontology_type)
    if ontology_type == "CHOP":
        match = re.search(r"CHOP.([0-9]{4})(.*).csv", ontology_name)
        return "sphn_chop_" + match.group(1) + "-4.ttl" , match.group(1), match.group(1)
    elif ontology_type == "SNOMED" and re.search(r"SnomedCT_InternationalRF2_PRODUCTION_([0-9]{8})(.*).zip", ontology_name) is not None:
        match = re.search(r"SnomedCT_InternationalRF2_PRODUCTION_([0-9]{8})(.*).zip", ontology_name)
        return "snomed-ct-" + match.group(1) + ".ttl" , match.group(1), match.group(1)
    elif ontology_type == "SNOMED" and re.search(r"SnomedCT_CH-INT_([0-9]{8})(.*).zip", ontology_name) is not None:
        match = re.search(r"SnomedCT_CH-INT_([0-9]{8})(.*).zip", ontology_name)
        return "snomed-ct-CH-" + match.group(1) + ".ttl" , match.group(1), match.group(1)
    elif ontology_type == "EMDN":
        match = re.search(r"([0-9-]{8,10})_\sEMDN(.*)\.xlsx", ontology_name)
        return "sphn_emdn_" + match.group(1) + "-1.ttl" , match.group(1), match.group(1)
    elif ontology_type == "ATC":
        match = re.search(r"([0-9]{4})\sATC.*\.xls(x?)", ontology_name)
        return "sphn_atc_" + match.group(1) + "-1.ttl" , match.group(1), match.group(1)
    elif ontology_type == "LOINC":
        match = re.search(r"Loinc_([0-9\.]{4})_([0-9]{4})_LoincTableCore\.zip", ontology_name)
        return "sphn_loinc_" + match.group(1) +"-1.ttl" , match.group(1), match.group(2)
    elif ontology_type == "ICD-10-GM":
        match = re.search(r"icd10gm([0-9]{4})syst(.*)Multilang\.zip", ontology_name)
        return "sphn_icd-10-gm_" + match.group(1) + "-3.ttl" , match.group(1), match.group(1)
    elif ontology_type == "UCUM":
        match = re.search(r"UcumCodes([0-9]{4})\.xlsx", ontology_name)
        return "sphn_ucum_" + match.group(1) + "-1.ttl" , match.group(1), match.group(1)
    elif ontology_type == "HGNC":
        match = re.search(r"hgnc_([0-9]{8})\.tsv\.gz", ontology_name)
        return "sphn_hgnc_" + match.group(1) + "-1.ttl" , match.group(1), match.group(1)
    elif ontology_type == "GENO":
        match = re.search(r"geno_([0-9]{8})\.owl\.gz", ontology_name)
        return "sphn_geno_" + match.group(1) + "-1.ttl" , match.group(1), match.group(1)
    elif ontology_type == "SO":
        match = re.search(r"so_([0-9]{8})\.owl\.gz", ontology_name)
        return "sphn_so_" + match.group(1) + "-1.ttl" , match.group(1), match.group(1)
    elif ontology_type == "EMDN":
        match = re.search(r"([0-9-]{10})_\sEMDN(.*)\.xlsx", ontology_name)
        return "sphn_emdn_" + match.group(1) + "-1.ttl" , match.group(1), match.group(1)
    elif ontology_type == "ORDO":
        match = re.search(r"ORDO_en_([0-9\.]{2,4})\.owl", ontology_name)
        return "sphn_ordo_" + match.group(1) + "-1.ttl" , match.group(1), match.group(1)
    elif ontology_type == "EDAM":
        match = re.search(r"EDAM_([0-9\.]{2,4})\.owl", ontology_name)
        return "sphn_edam_" + match.group(1) + "-1.ttl" , match.group(1), match.group(1)
    elif ontology_type == "ECO":
        match = re.search(r"eco_([0-9\.]{8})\.owl\.gz", ontology_name)
        return "sphn_eco_" + match.group(1) + "-1.ttl" , match.group(1), match.group(1)
    elif ontology_type == "EFO": 
        match = re.search(r"efo_v([0-9\.]{4,7})\.owl\.gz", ontology_name)
        return "sphn_efo_" + match.group(1) + "-1.ttl" , match.group(1), match.group(1)
    elif ontology_type == "GENEPIO":
        match = re.search(r"genepio_([0-9\.]{8})\.owl\.gz", ontology_name)
        return "sphn_genepio_" + match.group(1) + "-1.ttl" , match.group(1), match.group(1)
    elif ontology_type == "OBI":
        match = re.search(r"obi_([0-9\.]{8})\.owl\.gz", ontology_name)
        return "sphn_obi_" + match.group(1) + "-1.ttl" , match.group(1), match.group(1)
    elif ontology_type == "ICD-O-3":
        match = re.search(r"ICDO3_v([0-9]_[0-9])\.owl", ontology_name)
        return "sphn_icd-o-3-" + match.group(1) + "-1.ttl", match.group(1), match.group(1)
    elif ontology_type == "Oncotree":
        match = re.search(r"oncotree_([\d]{4}-[\d]{2}-[\d]{2}).json", ontology_name)
        return "sphn-oncotree-" + match.group(1) + "-1.ttl", match.group(1), match.group(1)
    else :
        raise "An ontology is found where not pattern for the name was specified!"



# read the files
fileList = pd.read_csv("sources.txt", names=['localpath'])

fileList['type'] = fileList['localpath'].apply(parseFolderStructure)

fileList['output_name'] = fileList['localpath'].apply(outputNameFromVersion)

fileList['version'] = fileList['localpath'].apply(outputVersionFromVersion)

fileList['year'] = fileList['localpath'].apply(outputYearFromVersion)


print("These are the files that have been detected and may serve as an input:")
print(fileList)

fileListNotInDatastore = fileList
fileListNewDatastore = fileList

try :
    fileListDatastore = pd.read_csv("datastore/sources.txt", names=['localpath', 'type', 'output_name', 'version', 'year'], sep="\t",)
    print("These are the files that are skipped in this run, as they are already in the datastore")
    print(fileListDatastore)

    fileListNotInDatastore = fileList[~fileList.localpath.isin(fileListDatastore.localpath)]

    print("This is the list that should be processed in this run:")
    print(fileListNotInDatastore)

    fileListNewDatastore = pd.concat([fileListNotInDatastore, fileListDatastore])
    print("This is the new datastore sources.txt file after this run:")
    print(fileListNewDatastore)
except :
    print ("no datastore file found or error, doing a full rebuild")
    fileListNotInDatastore = fileList
    fileListNewDatastore = fileList


os.remove("sources.txt")
fileListNotInDatastore.to_csv("sources/sources.txt", sep="\t", index=False)

if len(fileListNotInDatastore) > 0:
    fileListNewDatastore.to_csv("data/sources.txt", sep="\t", header=False, index=False)
