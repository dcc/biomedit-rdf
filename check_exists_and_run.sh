# !/bin/bash


# has been run like:  ./check_exists_and_run.sh CHOP ontology_pipelines/chop/ make help

# new format should just be ./check_exists_and_run.sh and it runs for everything

possible_ontologies=($(cd ontology_pipelines && ls -d */ | cut -f1 -d'/' && cd ..))
all_args=("$@")
#ontology=$1
#path_arg=$2
rest_args=("${all_args[@]:0}")


unpackSnomedBundle () {
    local originalInput=$1
    local input=$(basename ${originalInput})
    local prefix=$(dirname ${originalInput})
    local snomedCombinedFile=($(echo "$input" | grep "SnomedCT_CH-INT"))
    newFilePath=""
    if [[ $snomedCombinedFile == *"SnomedCT_CH-INT"* ]]
    then
        echo "found combined SNOMED file unpacking"
        unzip  $prefix/$snomedCombinedFile -d $prefix/SNOMEDCT_CH-INT_ZIP
        chmod -R 777 $prefix/SNOMEDCT_CH-INT_ZIP
        filePath=${snomedCombinedFile%/*}
        echo $filePath 
        paths=$(find $prefix/SNOMEDCT_CH-INT_ZIP -type f -name '*.zip' | tr "\n" "," | sed "s/,$//g")
        echo $paths
        OLDIFS=$IFS
        IFS=',' read -r -a newFilePathTemp <<< "$paths"
        newFilePathTemp2=( "${newFilePathTemp[@]/#/../../}" )
        IFS=$OLDIFS
        newFilePath=$( IFS=$','; echo "${newFilePathTemp2[*]}" )
        echo "Printing the file paths in $prefix/SNOMEDCT_CH-INT_ZIP"
        echo "$newFilePath"
       
    fi

}

for ontology in "${possible_ontologies[@]}"
do

    # Internal Field Separator might be space, that can be an issue for files with spaces (ATC)
    OLDIFS=$IFS
    IFS=$'\n'

    path_arg="ontology_pipelines/$ontology"
    prefix="../../"
    inputFiles=($(cat sources/sources.txt | grep $ontology | cut -d$'\t' -f1))
    target_filenames=($(cat sources/sources.txt | grep $ontology | cut -d$'\t' -f3))
    target_versions=($(cat sources/sources.txt | grep $ontology | cut -d$'\t' -f4))
    target_years=($(cat sources/sources.txt | grep $ontology | cut -d$'\t' -f5))

    IFS=$OLDIFS

    echo "Detected input files are:"
    echo "${inputFiles}"
    if [ -z "$inputFiles" ]
    then
        echo "no input found for $ontology, skipping"
    else 
        # if there are multiple files we need to run it for every input
        for ((i = 0; i < ${#inputFiles[@]}; ++i))
        do
            input=${inputFiles[$i]}
            target_filename=${target_filenames[$i]}
            target_version=${target_versions[$i]}
            target_year=${target_years[$i]}
            echo "input found for $ontology it is $input"
            # set the path right, as we need to go one up first.
            # interviene here for SNOMED if the target_filename contains a combhined SNOMED CH INT file
            possibleSnomedCombinedFile=($(echo "$input" | grep "SnomedCT_CH-INT"))
            echo $possibleSnomedCombinedFile
            if [[ $possibleSnomedCombinedFile == *"SnomedCT_CH-INT"* ]]
            then 
                echo "unpacking"
                unpackSnomedBundle $input . #$prefix
            fi 
            if [[ ! -z "$newFilePath" ]]
            then
                echo "applying the new SNOMED path for the bundle, they are already prefixed"
                prefixed_input="$newFilePath"
            else
                prefixed_input="$prefix$input"
            fi
            cd $path_arg
            echo  "Setting virtual environment"
            python3 -m venv ./venv
            chmod +x ./venv/bin/activate
            source ./venv/bin/activate
            echo "running the command ${rest_args[@]} in the path $path_arg with the parameter $prefixed_input"
            eval ${rest_args[@]} input=\"$prefixed_input\" target_version="$target_version" target_year="$target_year" target_filename="$target_filename"
            cd ../..
        done
    fi

done

echo "listing the data directory recursively"
ls -lshR data