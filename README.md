# Terminology Service
This repository contains the different ontology pipelines to convert the external terminologies to RDF, the terminology server source files, some auxiliary scripts to parse data and the whole CI/CD to run the ontology pipelines in a GitLab runner.
It is not intended that a user can run this repository on their local machine, although the individual pipelines can be run locally (see ontology_pipelines subfolder).

## Access RDF Files
The converted RDF files of the terminologies can be accessed at both the terminology server (http://terminology-server.dcc.sib.swiss) and the Terminology Service of the BioMedIT Portal (http://terminology.dcc.sib.swiss). More information can be found in the [SPHN documentation](https://sphn-semantic-framework.readthedocs.io/en/latest/sphn_framework/terminology_service.html).

## Referenced projects
Apache Jena Fuseki with additions (https://git.dcc.sib.swiss/dcc/jena-fuseki): Fuseki is used to test the correctness of the converted data. In the referenced project, also a Dockerfile is made availble. This can be used to host the Terminology Service locally. 

IHTSDO SNOMED OWL Toolkit (https://git.dcc.sib.swiss/dcc/snomed-owl-converter)

## License and Copyright
© Copyright 2025, Personalized Health Informatics Group (PHI), SIB Swiss Institute of Bioinformatics

The Terminology Service is licensed under the [GPLv3](https://www.gnu.org/licenses/gpl-3.0.en.html) (see [License](https://git.dcc.sib.swiss/dcc/biomedit-rdf/-/blob/master/LICENSE)).

## Contact
For any question or comment, please contact the Data Coordination Center FAIR Data team at [fair-data-team@sib.swiss](mailto:fair-data-team@sib.swiss).
