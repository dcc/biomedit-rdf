import sys
import pandas
import csv
from typing import Dict
from numpy import nan
from rdflib import Graph, RDF, RDFS, SKOS, DC, OWL, URIRef, Literal


PREFIX_MAP = {
    'rdf': RDF,
    'rdfs': RDFS,
    'owl': OWL,
    'skos': SKOS,
    'dc': DC,
    'oboInOwl': URIRef('http://www.geneontology.org/formats/oboInOwl#'),
    'RO': URIRef('http://purl.obolibrary.org/obo/RO_'),
    'IAO': URIRef('http://purl.obolibrary.org/obo/IAO_'),
    'NCBITaxon': URIRef('http://purl.obolibrary.org/obo/NCBITaxon_'),
    'hgnc': URIRef('https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/'),
    'genbank': URIRef('http://www.ncbi.nlm.nih.gov/nuccore/'),
    'refseq': URIRef('http://www.ncbi.nlm.nih.gov/refseq/?term='),
    'pubmed': URIRef("http://www.ncbi.nlm.nih.gov/pubmed/"),
    'eccode': URIRef('http://www.enzyme-database.org/query.php?ec='),
    'NCBIGene': URIRef(f"http://www.ncbi.nlm.nih.gov/gene/"),
    'ensembl': URIRef(f"http://ensembl.org/id/"),
    'MGI': URIRef(f"http://www.informatics.jax.org/accession/MGI:"),
    'ccds': URIRef(f"https://www.ncbi.nlm.nih.gov/CCDS/CcdsBrowse.cgi?REQUEST=CCDS&DATA="),
    'vega': URIRef(f"https://vega.archive.ensembl.org/Homo_sapiens/Gene/Summary?g="),
    'hgnc.genegroup': URIRef(f"https://www.genenames.org/cgi-bin/genefamilies/set/")
}


PREDICATE_MAP = {
    #'HGNC ID': '',
    'Approved symbol': RDFS.label,
    'Approved name': DC.description,
    'Status': RDFS.comment,
    'Alias symbols': URIRef('http://www.geneontology.org/formats/oboInOwl#Synonym'),
    'Chromosome': URIRef('http://purl.obolibrary.org/obo/RO_0002525'), # 'is subsequence of'
    'Accession numbers': URIRef('http://www.geneontology.org/formats/oboInOwl#hasDbXref'),
    'RefSeq IDs': URIRef('http://www.geneontology.org/formats/oboInOwl#hasDbXref'),
    #'Locus type': '',
    #'Locus group': '',
    'Alias names': URIRef('http://www.geneontology.org/formats/oboInOwl#Synonym'),
    'Enzyme IDs': URIRef('http://www.geneontology.org/formats/oboInOwl#hasDbXref'),
    'NCBI Gene ID': URIRef('http://www.geneontology.org/formats/oboInOwl#hasDbXref'),
    'Ensembl gene ID': URIRef('http://www.geneontology.org/formats/oboInOwl#hasDbXref'),
    'Mouse genome database ID': URIRef('http://www.geneontology.org/formats/oboInOwl#hasDbXref'),
    'Pubmed IDs': URIRef('http://purl.obolibrary.org/obo/IAO_0000142'), # 'mentions'
    'Gene group ID': URIRef('http://purl.obolibrary.org/obo/RO_0002350'), # 'member of'
    # 'Gene group name': '',
    'CCDS IDs': URIRef('http://www.geneontology.org/formats/oboInOwl#hasDbXref'),
    'Vega IDs': URIRef('http://www.geneontology.org/formats/oboInOwl#hasDbXref'),
    'Taxon': URIRef('http://purl.obolibrary.org/obo/RO_0002162'),
}

DELIMITER = {
    'Alias symbols': ',',
    'Accession numbers': ',',
    'RefSeq IDs': ',',
    'Alias names': '", ',
    'Enzyme IDs': ',',
    'NCBI Gene ID': ',',
    'Ensembl gene ID': ',',
    'Mouse genome database ID': ',',
    'Pubmed IDs': ',',
    'CCDS IDs': ',',
    'Vega IDs': ',',
}


def prepare_rdf_graph(prefix_map: Dict, version: str) -> Graph:
    """
    Prepare an instance of ``rdflib.Graph`` with the required metadata.

    Args:
        prefix_map: A dictionary containing prefix to IRI mappings
        version: The version of the HGNC data

    Returns:
        An instance of ``rdflib.Graph``

    """
    graph = Graph()
    for prefix, iri in prefix_map.items():
        graph.bind(prefix, iri)
    namespace = URIRef("https://biomedit.ch/rdf/sphn-resource/hgnc/")
    namespace_version = URIRef(f"https://biomedit.ch/rdf/sphn-resource/hgnc/{version}")
    copyright = Literal(
        "RDF version of HUGO Gene Nomenclature Committee (HGNC) is developed by the SPHN DCC (PHI, SIB Swiss Institute of Bioinformatics). "
        "The copyright follows the instructions provided by the EMBL-EBI, Wellcome Genome Campus, Hinxton, CB10 1SD. For more details on the usage rights, please consult the EMBL-EBI terms of use [(https://www.ebi.ac.uk/about/terms-of-use)](https://www.ebi.ac.uk/about/terms-of-use). "
    )
    graph.add((namespace, DC.rights, copyright))
    graph.add((namespace, OWL.versionIRI, namespace_version))
    graph.add((namespace, RDF.type, OWL.Ontology))
    # add the main class as well:
    root_node = URIRef(namespace + "HGNC")
    graph.add((root_node, RDF.type, RDFS.Class))
    graph.add((root_node, RDFS.label, Literal("HGNC")))

    return graph, root_node


def parse_data(filename: str, version: str) -> Graph:
    """
    Parse HGNC data from ``filename`` and prepare an RDF representation of the data.

    Args:
        filename: The filename
        version: The version of the HGNC data

    Returns:
        An instance of ``rdflib.Graph``

    """
    gene_groups = set()
    taxon_predicate = PREDICATE_MAP['Taxon']
    taxon_object = URIRef('http://purl.obolibrary.org/obo/NCBITaxon_9606')
    graph, root_node = prepare_rdf_graph(prefix_map=PREFIX_MAP, version=version)
    with open(filename) as file:
        reader = csv.DictReader(file, delimiter='\t', quoting=csv.QUOTE_NONE)
        for row in reader:
            subject = URIRef(f"https://www.genenames.org/data/gene-symbol-report/#!/hgnc_id/{row['HGNC ID'].replace('HGNC:', '')}")
            graph.add((subject, RDF.type, OWL.Class))
            graph.add((subject, RDFS.subClassOf, root_node))
            graph.add((subject, taxon_predicate, taxon_object))
            for key, value in row.items():
                if key in PREDICATE_MAP:
                    predicate = PREDICATE_MAP[key]
                    if key in DELIMITER and DELIMITER[key]:
                        value_list = value.split(DELIMITER[key])
                    else:
                        value_list = [value]
                    for value in value_list:
                        value = value.strip()
                        if not value:
                            continue
                        if key == 'Pubmed IDs':
                            object = URIRef(f"http://www.ncbi.nlm.nih.gov/pubmed/{value.strip()}")
                            graph.add((object, predicate, subject))
                        elif key == 'Accession numbers':
                            object = URIRef(f"http://www.ncbi.nlm.nih.gov/nuccore/{value.strip()}")
                            graph.add((subject, predicate, object))
                        elif key == 'RefSeq IDs':
                            object = URIRef(f"http://www.ncbi.nlm.nih.gov/refseq/?term={value.strip()}")
                            graph.add((subject, predicate, object))
                        elif key == 'Enzyme IDs':
                            object = URIRef(f"http://www.enzyme-database.org/query.php?ec={value.strip()}")
                            graph.add((subject, predicate, object))
                        elif key == 'NCBI Gene ID':
                            object = URIRef(f"http://www.ncbi.nlm.nih.gov/gene/{value.strip()}")
                            graph.add((subject, predicate, object))
                        elif key == 'Ensembl gene ID':
                            object = URIRef(f"http://ensembl.org/id/{value.strip()}")
                            graph.add((subject, predicate, object))
                        elif key == 'Mouse genome database ID':
                            object = URIRef(f"http://www.informatics.jax.org/accession/{value.strip()}")
                            graph.add((subject, predicate, object))
                        elif key == 'CCDS IDs':
                            object = URIRef(f"https://www.ncbi.nlm.nih.gov/CCDS/CcdsBrowse.cgi?REQUEST=CCDS&DATA={value.strip()}")
                            graph.add((subject, predicate, object))
                        elif key == 'Vega IDs':
                            object = URIRef(f"https://vega.archive.ensembl.org/Homo_sapiens/Gene/Summary?g={value.strip()}")
                            graph.add((subject, predicate, object))
                        elif key == "Approved name":
                            object = Literal(value)
                            graph.add((subject, predicate, object))
                        elif key == 'Gene group ID':
                            gene_group_ids = value.split('|')
                            gene_group_names = row['Gene group name'].split('|')
                            if gene_group_ids:
                                for group_id, group_name in zip(gene_group_ids, gene_group_names):
                                    if group_id:
                                        object = URIRef(f"https://www.genenames.org/cgi-bin/genefamilies/set/{group_id.strip()}")
                                        if object not in gene_groups:
                                            graph.add((object, RDF.type, OWL.Class))
                                            graph.add((object, RDFS.label, Literal(group_name.strip())))
                                            graph.add((object, RDFS.subClassOf, root_node))
                                            gene_groups.add(object)
                                        graph.add((subject, PREDICATE_MAP['Gene group ID'], object))
                        else:
                            if key == 'Alias names':
                                value = value.replace('"', '')
                            object = Literal(value)
                            graph.add((subject, predicate, object))
    return graph


def serialize_rdf_graph(graph: Graph, filename = "sphn-hgnc.ttl") -> None:
    """
    Write triples in ``rdflib.Graph`` to a file.

    Args:
        graph: An instance of ``rdflib.Graph``
        filename: The output filename

    """
    file = open(filename, "w")
    file.write(graph.serialize(format="turtle").decode("utf-8"))
    file.close()


if __name__ == '__main__':
    version = sys.argv[1]
    input_filename = sys.argv[2]
    output_filename = sys.argv[3]
    graph = parse_data(filename=input_filename, version=version)
    serialize_rdf_graph(graph, filename=output_filename)


