# HGNC Pipeline

## Getting the data

Following are the properties that are required for each gene in HUGO:
- HGNC official field name
- HGNC ID
- Approved symbol
- Approved name
- Status
- Locus type
- Locus group
- Alias symbols
- Alias names
- Accession numbers
- Chromosome
- Enzyme ID
- NCBI Gene ID
- Ensembl gene ID
- Mouse genome database ID
- Pubmed ID
- RefSeq ID
- Gene group ID
- Gene group name
- CCDC ID
- Vega ID

The raw data file with the required properties can be downloaded via the following URL: https://www.genenames.org/cgi-bin/download/custom?col=gd_hgnc_id&col=gd_app_sym&col=gd_app_name&col=gd_status&col=gd_aliases&col=gd_pub_chrom_map&col=gd_pub_acc_ids&col=gd_pub_refseq_ids&col=gd_locus_type&col=gd_locus_group&col=gd_name_aliases&col=gd_enz_ids&col=gd_pub_eg_id&col=gd_pub_ensembl_id&col=gd_mgd_id&col=gd_pubmed_ids&col=family.id&col=family.name&col=gd_ccds_ids&col=gd_vega_ids&status=Approved&status=Entry%20Withdrawn&hgnc_dbtag=on&order_by=gd_app_sym_sort&format=text&submit=submit


## Copyright statement
The copyright follows the instructions provided by the EMBL-EBI, Wellcome Genome Campus, Hinxton, CB10 1SD. For more details on the usage rights, please consult the EMBL-EBI terms of use (https://www.ebi.ac.uk/about/terms-of-use).

