# GENO Ontology Pipeline

This pipeline converts OWL XML representation of Genotype Ontology (GENO) to an RDF Turtle representation.

The GENO OWL can be downloaded from http://purl.obolibrary.org/obo/geno.owl.

## Copyright statement
GENO is published by the Monarch initiative and is an open-source ontology, implemented in OWL2 under a Creative Commons 4.0 BY license (https://github.com/monarch-initiative/GENO-ontology/).

