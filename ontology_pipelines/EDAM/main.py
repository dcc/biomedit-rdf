import sys
from typing import Dict
from rdflib import Graph, RDF, RDFS, SKOS, DC, OWL, URIRef, Literal


PREFIX_MAP = {
    'OBO': 'http://purl.obolibrary.org/obo#',
    'BFO': 'http://purl.obolibrary.org/obo/BFO_',
    'FALDO': 'http://biohackathon.org/resource/faldo#',
    'SO': 'http://purl.obolibrary.org/obo/SO_',
    'IAO': 'http://purl.obolibrary.org/obo/IAO_',
    'GENO': 'http://purl.obolibrary.org/obo/GENO_',
    'HP': 'http://purl.obolibrary.org/obo/HP_',
    'RO': 'http://purl.obolibrary.org/obo/RO_',
    'UBERON': 'http://purl.obolibrary.org/obo/UBERON_',
    'ENVO': 'http://purl.obolibrary.org/obo/ENVO_',
    'GO': 'http://purl.obolibrary.org/obo/GO_',
    'UPHENO': 'http://purl.obolibrary.org/obo/UPHENO_',
    'OBI': 'http://purl.obolibrary.org/obo/OBI_',
    'OMO': 'http://purl.obolibrary.org/obo/OMO_',
    'NCBITaxon': 'http://purl.obolibrary.org/obo/NCBITaxon_',
    'PATO': 'http://purl.obolibrary.org/obo/PATO_',
    'PCO': 'http://purl.obolibrary.org/obo/PCO_',
    'CHEBI': 'http://purl.obolibrary.org/obo/CHEBI_',
    'ZP': 'http://purl.obolibrary.org/obo/ZP_',
    'MP': 'http://purl.obolibrary.org/obo/MP_',
    'WBPhenotype': 'http://purl.obolibrary.org/obo/WBPhenotype_',
    'CL': 'http://purl.obolibrary.org/obo/CL_',
    'CLO': 'http://purl.obolibrary.org/obo/CLO_',
    'sphn-edam': 'https://biomedit.ch/rdf/sphn-resource/edam/',
    'EDAM': 'http://edamontology.org/'
}

EDAM_main_classes = [URIRef("http://edamontology.org/data_0006"),
                        URIRef("http://edamontology.org/format_1915"),
                        URIRef("http://edamontology.org/operation_0004"),
                        URIRef("http://edamontology.org/topic_0003")] # and owl deprecated


def prepare_rdf_graph(graph: Graph, prefix_map: Dict, version: str) -> Graph:
    """
    Prepare an instance of ``rdflib.Graph`` with the required metadata.

    Args:
        prefix_map: A dictionary containing prefix to IRI mappings
        version: The version of the geno data

    Returns:
        An instance of ``rdflib.Graph``

    """
    for prefix, iri in prefix_map.items():
        graph.bind(prefix, iri)
    namespace = URIRef("https://biomedit.ch/rdf/sphn-resource/edam/")
    namespace_version = URIRef(f"https://biomedit.ch/rdf/sphn-resource/edam/{version}")

    copyright = Literal(
        "SPHN DCC (PHI, SIB Swiss Institute of Bioinformatics) adapted model of the EDAM OWL model:EDAM is a domain ontology of data analysis and data management in bio- and other sciences, and science-based applications. It comprises concepts related to analysis, modelling, optimisation, and data life-cycle. Targetting usability by diverse users, the structure of EDAM is relatively simple, divided into 4 main sections: Topic, Operation, Data (incl. Identifier), and Format. EDAM is particularly suitable for semantic annotations and categorisation of diverse resources related to data analysis and management: e.g. tools, workflows, learning materials, or standards. EDAM is also useful in data management itself, for recording provenance metadata of processed data. As a reference article (citation), please use https://doi.org/10.7490/f1000research.1118900.1"
    )
    graph.add((namespace, DC.rights, copyright))
    graph.add((namespace, OWL.versionIRI, namespace_version))
    graph.add((namespace, RDF.type, OWL.Ontology))
    graph.add((namespace, DC.license, URIRef("https://creativecommons.org/licenses/by/4.0/")))

    return graph

def normalize_prefixes(graph: Graph, prefix_map: Dict = None) -> None:
    """
    Normalize prefix to IRI bindings as specified in ``prefix_map``.

    Args:
        graph: An instance of rdflib.Graph
        prefix_map: A dictionary of prefix to IRI mappings

    """
    if prefix_map:
        for prefix, iri in prefix_map.items():
            graph.bind(prefix, iri, override=True)

def remove_original_geno_header(graph):
    graph.remove((URIRef("http://edamontology.org"), None, None))

if __name__ == '__main__':
    """
    Automatically converts the graph into ttl format and adds versioning information

    1. input file
    2. output file
    3. file version
    """
    graph = Graph()
    graph.parse(sys.argv[1])
    graph = prepare_rdf_graph(graph, PREFIX_MAP, sys.argv[3])
    remove_original_geno_header(graph)
    graph.serialize(sys.argv[2], format='ttl')

