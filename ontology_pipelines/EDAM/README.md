# EDAM Ontology Pipeline

This pipeline is just to convert OWL XML representation of Edamontology (EDAM) to an RDF Turtle representation.

The EDAM OWL can be downloaded from https://github.com/edamontology/edamontology/tree/main/releases
