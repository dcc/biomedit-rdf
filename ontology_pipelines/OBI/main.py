import sys
from typing import Dict
from rdflib import Graph


PREFIX_MAP = {
    'OBO': 'http://purl.obolibrary.org/obo#',
    'BFO': 'http://purl.obolibrary.org/obo/BFO_',
    'FALDO': 'http://biohackathon.org/resource/faldo#',
    'SO': 'http://purl.obolibrary.org/obo/SO_',
    'IAO': 'http://purl.obolibrary.org/obo/IAO_',
    'GENO': 'http://purl.obolibrary.org/obo/GENO_',
    'HP': 'http://purl.obolibrary.org/obo/HP_',
    'RO': 'http://purl.obolibrary.org/obo/RO_',
    'UBERON': 'http://purl.obolibrary.org/obo/UBERON_',
    'ENVO': 'http://purl.obolibrary.org/obo/ENVO_',
    'GO': 'http://purl.obolibrary.org/obo/GO_',
    'UPHENO': 'http://purl.obolibrary.org/obo/UPHENO_',
    'OBI': 'http://purl.obolibrary.org/obo/OBI_',
    'OMO': 'http://purl.obolibrary.org/obo/OMO_',
    'NCBITaxon': 'http://purl.obolibrary.org/obo/NCBITaxon_',
    'PATO': 'http://purl.obolibrary.org/obo/PATO_',
    'PCO': 'http://purl.obolibrary.org/obo/PCO_',
    'CHEBI': 'http://purl.obolibrary.org/obo/CHEBI_',
    'ZP': 'http://purl.obolibrary.org/obo/ZP_',
    'MP': 'http://purl.obolibrary.org/obo/MP_',
    'WBPhenotype': 'http://purl.obolibrary.org/obo/WBPhenotype_',
    'CL': 'http://purl.obolibrary.org/obo/CL_',
    'CLO': 'http://purl.obolibrary.org/obo/CLO_',
    'APOLLO_SV': 'http://purl.obolibrary.org/obo/APOLLO_SV_',
    'PR': 'http://purl.obolibrary.org/obo/PR_',
    'OBA': 'http://purl.obolibrary.org/obo/OBA_',
    'UO': 'http://purl.obolibrary.org/obo/UO_',
    'COB': 'http://purl.obolibrary.org/obo/COB_',
    'OGMS': 'http://purl.obolibrary.org/obo/OGMS_',
    'OMIABIS': 'http://purl.obolibrary.org/obo/OMIABIS_',
    'CHMO': 'http://purl.obolibrary.org/obo/CHMO_',
    'IDO': 'http://purl.obolibrary.org/obo/IDO_',
    'GAZ': 'http://purl.obolibrary.org/obo/GAZ_',
    'OPL': 'http://purl.obolibrary.org/obo/OPL_',
    'REO': 'http://purl.obolibrary.org/obo/REO_',
    'VO': 'http://purl.obolibrary.org/obo/VO_',
    'OMRSE': 'http://purl.obolibrary.org/obo/OMRSE_'
}


def prepare_rdf_graph(graph: Graph, prefix_map: Dict, version: str) -> Graph:
    """
    Prepare an instance of ``rdflib.Graph`` with the required metadata.

    Args:
        prefix_map: A dictionary containing prefix to IRI mappings
        version: The version of the ontology

    Returns:
        An instance of ``rdflib.Graph``

    """
    normalize_prefixes(graph, prefix_map)
    return graph


def normalize_prefixes(graph: Graph, prefix_map: Dict = None) -> None:
    """
    Normalize prefix to IRI bindings as specified in ``prefix_map``.

    Args:
        graph: An instance of rdflib.Graph
        prefix_map: A dictionary of prefix to IRI mappings

    """
    if prefix_map:
        for prefix, iri in prefix_map.items():
            graph.bind(prefix, iri, override=True)


if __name__ == '__main__':
    graph = Graph()
    graph.parse(sys.argv[1])
    graph = prepare_rdf_graph(graph=graph, prefix_map=PREFIX_MAP, version=sys.argv[3])
    graph.serialize(sys.argv[2], format='ttl')

