# UCUM codes in RDF

Contains the script to transform UCUM codes provided by the Regenstrief Institute into RDF by SPHN.


## Copyright statement
The copyright follows the instructions provided by Regenstrief Institute. UCUM is Copyright © 1999-2013 Regenstrief Institute, Inc. and The UCUM Organization, Indianapolis, IN. All rights reserved. See [TermsOfUse](http://unitsofmeasure.org/trac/wiki/TermsOfUse).