# Python script: translate UCUM into RDF

import pandas as pd
import sys
import rdflib

from rdflib import Graph, Literal
from rdflib.namespace import RDFS, RDF, OWL

# UCUM is currently generated based on the SPHN dataset release
# Year Version correspond, respectively, to the SPHN dataset year release and to its version
YEAR = sys.argv[2]
VERSION = sys.argv[3]

NAMESPACE = "https://biomedit.ch/rdf/sphn-resource/ucum/"
NAMESPACE_VERSION = "https://biomedit.ch/rdf/sphn-resource/ucum/" + YEAR + "/" + VERSION

# Dict of unsafe and reserved characters to string for better uris:
uri_character_mapping = {'.': 'dot', '%': 'percent', '{': 'cbl', '}': 'cbr', '/': 'per', '#':'nb',
                         '[': 'sbl', ']': 'sbr', '\'': 'apo', '(': 'rbl', ')': 'rbr', '*': 'exp'}


def import_excel(file_name):
    df = pd.read_excel(file_name, header=1)
    return df


def create_rdf(data):
    # create RDF graph:
    graph = Graph()

    # Header:
    graph.bind("rdfs", RDFS)
    graph.bind("rdf", RDF)
    graph.bind("ucum", NAMESPACE)
    graph.bind("owl", OWL)

    # set version
    ucum = rdflib.term.URIRef(NAMESPACE)
    ucum_version = rdflib.term.URIRef(NAMESPACE_VERSION)
    graph.add((ucum, OWL.versionIRI, ucum_version))
    graph.add((ucum, RDF.type, OWL.Ontology))

    # add copyright information
    copyright = Literal(
        "RDF version of UCUM (https://ucum.org/trac), developed by the SPHN DCC (PHI, SIB Swiss Institute of Bioinformatics). "
        "The copyright follows UCUM©  Copyright 1999-2022 Regenstrief Institute, Inc. All rights reserved. Licensed under the UCUM License, Version 1.0; You may obtain a copy of the License at https://unitsofmeasure.org/license.")
    graph.add((ucum, RDFS.comment, copyright))

    root_node = rdflib.term.URIRef(NAMESPACE + "UCUM")
    graph.add((root_node, RDF.type, RDFS.Class))
    graph.add((root_node, RDFS.label, Literal("UCUM")))

    # parse LOINC classification
    for index, row in data.iterrows():
        # URI is generated from the code using the uri_character_mapping that change reserved characters into a string
        encoded_uri = ''.join([uri_character_mapping.get(i, i) for i in list(row['UCUM_CODE'])])
        encoded_uri = encoded_uri.replace(" ", "")

        subject = rdflib.term.URIRef(NAMESPACE + encoded_uri)

        graph.add((subject, RDF.type, RDFS.Class))
        graph.add((subject, RDFS.subClassOf, root_node))
        ucum_label = Literal(row["UCUM_CODE"])
        graph.add((subject, RDFS.label, ucum_label))

        ucum_comment = Literal(row["Description of the Unit (using UCUM descriptions where they exist)"])
        graph.add((subject, RDFS.comment, ucum_comment))

    return graph


def export_turtle_file(graph):
    file_name = "sphn_ucum_" + YEAR +"-" + VERSION + ".ttl"
    file = open(file_name, "w")
    file.write(graph.serialize(format="turtle").decode("utf-8"))
    file.close()


if __name__ == '__main__':
    # path to file must be given as argument #1
    filePath = sys.argv[1]
    data = import_excel(filePath)
    graph = create_rdf(data)
    export_turtle_file(graph)
