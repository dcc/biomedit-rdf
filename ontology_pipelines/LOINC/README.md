# LOINC classification in RDF

Contains the script to transform LOINC classification into RDF by SPHN.
Download from: https://loinc.org/download/loinc-table-core/

## Copyright statement
LOINC ([http://loinc.org](http://loinc.org)). LOINC is copyright © 1995-2021, Regenstrief Institute, Inc. and the Logical Observation Identifiers Names and Codes (LOINC) Committee and is available at no cost under the license at [http://loinc.org/license](http://loinc.org/license). LOINC® is a registered United States trademark of Regenstrief Institute, Inc.
