# Python script: translate LOINC into RDF

import pandas as pd
import sys
import rdflib

from rdflib import Graph, Literal
from rdflib.namespace import RDFS, RDF, OWL

# release version of the LOINC given as argument #2
LOINC_VERSION = sys.argv[2]
# version of the LOINC RDF given as argument #3
VERSION = sys.argv[3]
# YEAR of the LOINC RDF given as argument #4
YEAR = sys.argv[4]

NAMESPACE = "https://loinc.org/rdf/"
SPHN_NAMESPACE = "https://biomedit.ch/rdf/sphn-resource/loinc/"
NAMESPACE_VERSION = "https://biomedit.ch/rdf/sphn-resource/loinc/"+LOINC_VERSION+"/"+VERSION


def import_excel(file_name):
    df = pd.read_csv(file_name)
    return df

def create_rdf(data):
    # create RDF graph:
    graph = Graph()

    # Header:
    graph.bind("rdfs", RDFS)
    graph.bind("rdf", RDF)
    graph.bind("loinc", NAMESPACE)
    graph.bind("owl", OWL)
    graph.bind("sphn-loinc", SPHN_NAMESPACE)

    # set version
    sphn_loinc = rdflib.term.URIRef(SPHN_NAMESPACE)
    sphn_loinc_version = rdflib.term.URIRef(NAMESPACE_VERSION)
    graph.add((sphn_loinc, OWL.versionIRI, sphn_loinc_version))
    graph.add((sphn_loinc, RDF.type, OWL.Ontology))

    # add copyright information
    copyright = Literal(
        "RDF version of LOINC (http://loinc.org), developed by the SPHN DCC (PHI, SIB Swiss Institute of Bioinformatics). "
        "The copyright follows LOINC© 1995-"+ YEAR + ", Regenstrief Institute, Inc.")
    graph.add((sphn_loinc, RDFS.comment, copyright))

    #set root node (LOINC) to which all codes will be subclasses
    root_node = rdflib.term.URIRef(SPHN_NAMESPACE + "LOINC")
    graph.add((root_node, RDF.type, RDFS.Class))
    graph.add((root_node, RDFS.label, Literal("LOINC")))

    # parse LOINC classification
    for index, row in data.iterrows():
        subject = rdflib.term.URIRef(NAMESPACE + row['LOINC_NUM'])
        graph.add((subject, RDF.type, RDFS.Class))
        loinc_label = Literal(row["LONG_COMMON_NAME"])
        graph.add((subject, RDFS.label, loinc_label))

        # Currently, the LOINC codes are added as a flat list. There's no hierarchy stored in the RDF besides the root node
        graph.add((subject, RDFS.subClassOf, root_node))

        addMetadata(graph, subject, row["COMPONENT"], "hasComponent")
        addMetadata(graph, subject, row["PROPERTY"], "hasProperty")
        addMetadata(graph, subject, row["TIME_ASPCT"], "hasTime")
        addMetadata(graph, subject, row["SYSTEM"], "hasSystem")
        addMetadata(graph, subject, row["SCALE_TYP"], "hasScaleType")
        addMetadata(graph, subject, row["METHOD_TYP"], "hasMethodType")

    return graph
        
def addMetadata(graph, subject, metadata, property):
        if not pd.isnull(metadata):
            loinc_metadata = Literal(metadata)
            relation = rdflib.term.URIRef(SPHN_NAMESPACE + property)
            graph.add((subject, relation, loinc_metadata))

def export_turtle_file(graph):
    file_name = "./sphn_loinc_" + LOINC_VERSION +"-" + VERSION + ".ttl"
    file = open(file_name, "w")
    file.write(graph.serialize(format="turtle").decode("utf-8"))
    file.close()

if __name__ == '__main__':
    # path to file must be given as argument #1
    filePath = sys.argv[1]
    data = import_excel(filePath)
    print("parsing in progress...")
    graph = create_rdf(data)
    print("export in progress...")
    export_turtle_file(graph)
    print("Done.")