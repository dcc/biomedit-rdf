from enum import Enum

class Organizations(Enum):
    " Enum class for organisations integrated in the metadata "
    BFS = "Bundesamt für Statistik"
    BFS_URL = "https://www.bfs.admin.ch/"
    BFARM = "Bundesinstitut für Arzneimittel und Medizinprodukte"
    BFARM_URL = "https://www.bfarm.de/"
    SPHN_DCC = "SPHN Data Coordination Center"
    SPHN_DCC_URL = "https://sphn.ch/network/data-coordination-center/"

class BfarmVersion(Enum):
    """ 
    ENUM Class to map the BFARM version so the BFS & SPHN version
    """
    YEAR_2014 = 2012
    YEAR_2015 = 2014
    YEAR_2016 = 2014
    YEAR_2017 = 2016
    YEAR_2018 = 2016
    YEAR_2019 = 2018
    YEAR_2020 = 2018
    YEAR_2021 = 2021
    YEAR_2022 = 2022
    YEAR_2023 = 2022
    YEAR_2024 = 2022
    YEAR_2025 = 2024
    