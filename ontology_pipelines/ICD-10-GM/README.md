# ICD-10-GM classification in RDF

Contains the script to transform ICD-10-GM classification into RDF by SPHN.

## Copyright statement
The ICD-10-GM RDF file has been produced using the ICD-10-GM machine-readable version of the German Institute of Medical Documentation and Information (DIMDI) which belongs to the BfArM.