# Python script: translate ICD-10-GM into RDF
import sys
import pandas as pd
import rdflib
from rdflib import Graph, RDF, RDFS, Literal, OWL, FOAF, PROV, XSD
from enum import Enum
from enums import Organizations, BfarmVersion
#
# isodate==0.6.1
YEAR = sys.argv[1]
# version of ICD-10-GM RDF given as argument #2
VERSION = sys.argv[2]
ICD10_NAMESPACE = "https://biomedit.ch/rdf/sphn-resource/icd-10-gm/"
NAMESPACE_VERSION = "https://biomedit.ch/rdf/sphn-resource/icd-10-gm/" + YEAR + "/" + VERSION
NAMESPACE = None

list_block = {}

def create_rdf(codes, groups, chapters, version, **kwargs):
    # create RDF graph:
    graph = Graph()
    global NAMESPACE
    # header:
    graph.bind("rdfs", RDFS)
    graph.bind("rdf", RDF)
    graph.bind("owl", OWL)
    graph.bind("foaf", FOAF)
    if len(version) > 0:
        NAMESPACE = "https://biomedit.ch/rdf/sphn-resource/icd-10-gm/" + str(version) + "/"
    else:
        NAMESPACE = "https://biomedit.ch/rdf/sphn-resource/icd-10-gm/"
    graph.bind("icd-10-gm", NAMESPACE)
    PAV = rdflib.Namespace("http://purl.org/pav/")
    graph.bind("pav", PAV)

    # set version
    sphn_icd10 = rdflib.term.URIRef(ICD10_NAMESPACE)
    sphn_icd10_version = rdflib.term.URIRef(NAMESPACE_VERSION)
    graph.add((sphn_icd10, OWL.versionIRI, sphn_icd10_version))
    graph.add((sphn_icd10, RDF.type, OWL.Ontology))

    SPHN_DCC = rdflib.term.URIRef(Organizations.SPHN_DCC_URL.value)
    BFS = rdflib.term.URIRef(Organizations.BFS_URL.value)

    # dynamically extracts the corresponding BFARM version for a given year
    bfarm_year = _get_bfarm_version(year=YEAR)
    BFARM = rdflib.term.URIRef(Organizations.BFARM_URL.value)
    organizations_iri_dict = {Organizations.SPHN_DCC.name:(Organizations.SPHN_DCC,SPHN_DCC), Organizations.BFS.name:(Organizations.BFS,BFS), Organizations.BFARM.name:(Organizations.BFARM,BFARM)}

    for key, value in organizations_iri_dict.items():
        _add_metadata(graph=graph, orga_enum=key, orga_tuple=value, year=YEAR, namespace=ICD10_NAMESPACE, PAV=PAV, bfarm_year=bfarm_year)

    # after the function
    graph.add((sphn_icd10, PAV.derivedFrom, rdflib.term.URIRef(ICD10_NAMESPACE+ Organizations.SPHN_DCC.name)))
    graph.add((rdflib.term.URIRef(ICD10_NAMESPACE+ Organizations.SPHN_DCC.name), PAV.derivedFrom, rdflib.term.URIRef(ICD10_NAMESPACE+ Organizations.BFS.name)))

    graph.add((rdflib.term.URIRef(ICD10_NAMESPACE+ Organizations.BFS.name), PAV.derivedFrom, rdflib.term.URIRef(ICD10_NAMESPACE+ Organizations.BFARM.name) ))

    # add copyright information
    copyright = Literal(
        "RDF version of ICD-10-GM (https://www.bfarm.de/), developed by the SPHN DCC (PHI, SIB Swiss Institute of Bioinformatics). "
        "The copyright follows instructions by the German federal Institute for pharmaceuticals and medical products (BfArM)")
    graph.add((sphn_icd10, RDFS.comment, copyright))

    fr_labels = {}
    if 'fr_codes' in kwargs and kwargs['fr_codes'] is not None:
        fr_labels.update(parse_other_labels_for_codes(kwargs['fr_codes']))
    if 'fr_groups' in kwargs and kwargs['fr_groups'] is not None:
        fr_labels.update(parse_other_labels_for_groups(kwargs['fr_groups']))
    if 'fr_chapters' in kwargs and kwargs['fr_chapters'] is not None:
        fr_labels.update(parse_other_labels_for_chapters(kwargs['fr_chapters']))

    # set root node (ICD-10-GM) to which all codes will be subclasses
    root_node = rdflib.term.URIRef(ICD10_NAMESPACE + "ICD-10-GM")
    graph.add((root_node, RDF.type, RDFS.Class))
    graph.add((root_node, RDFS.label, Literal("ICD-10-GM")))

    # parse ICD-10-GM chapters (level 1 in hierarchy)
    for index, row in chapters.iterrows():
        chapter = rdflib.term.URIRef(NAMESPACE + str(row[0]).zfill(2))
        add_new_class(chapter, root_node, row[1], graph, fr_labels, root_node)

    # parse ICD-10-GM groups (level 2 in hierarchy) - called block
    for index, row in groups.iterrows():
        block = rdflib.term.URIRef(NAMESPACE + str(row[0])+"-"+str(row[1]))
        add_new_class(block, rdflib.term.URIRef(NAMESPACE + str(row[2]).zfill(2)), row[3], graph, fr_labels, root_node)

        # dict to make hierarchy easier between block and category
        list_block[row[0]] = block

    # classification row by row
    for index, row in codes.iterrows():
        if row[0] == 3: # level 3 in hierarchy
            category = rdflib.term.URIRef(NAMESPACE + str(row[5]))
            add_new_class(category, list_block[row[4]], row[8], graph, fr_labels, root_node)

        elif row[0] == 4: # level 4 in hierarchy
            subcateg = rdflib.term.URIRef(NAMESPACE+ str(row[5]))
            add_new_class(subcateg, category, row[8], graph, fr_labels, root_node)

        elif row[0] == 5: # level 5 in hierarchy
            subsubcateg = rdflib.term.URIRef(NAMESPACE+ str(row[5]))
            add_new_class(subsubcateg, subcateg, row[8], graph, fr_labels, root_node)

    return graph


def parse_other_labels_for_codes(codes):
    code_labels = {}
    for index, row in codes.iterrows():
        if row[5].endswith("*") or row[5].endswith("!") or row[5].endswith("†"):
            code = rdflib.term.URIRef(NAMESPACE + str(row[5][:-1]))
        elif row[5].endswith(".-"):
            code = rdflib.term.URIRef(NAMESPACE + str(row[5][:-2]))
        else:
            code = rdflib.term.URIRef(NAMESPACE + str(row[5]))
        label = row[8]
        code_labels[code] = label
    return code_labels


def parse_other_labels_for_groups(groups):
    group_labels = {}
    for index, row in groups.iterrows():
        group = rdflib.term.URIRef(f"{NAMESPACE}{row[0]}-{row[1]}")
        label = row[3]
        group_labels[group] = label
    return group_labels


def parse_other_labels_for_chapters(chapters):
    chapter_labels = {}
    for index, row in chapters.iterrows():
        chapter = rdflib.term.URIRef(NAMESPACE + str(row[0]).zfill(2))
        label = row[1]
        chapter_labels[chapter] = label
    return chapter_labels


def add_new_class(subject, object, row_element, graph, fr_labels = {}, root_node=None):
    # Remove additional identifiers from codes:
    # The additional identifiers "*", "!" and "†" described can be traced back
    # to the regulations of the ICD -10-GM itself and do not have to be
    # specified in the data transmission, as per
    # https://www.aerzteblatt.de/archiv/81727/Ambulante-Kodierrichtlinien-Mehrfachkodierung-Wenn-ein-Kode-nicht-ausreicht
    if subject.endswith("*") or subject.endswith("!") or subject.endswith("†"):
        subject = subject[:-1]
    if object.endswith("*") or object.endswith("!") or object.endswith("†"):
        object = object[:-1]

    subject_iri = rdflib.term.URIRef(subject)
    object_iri = rdflib.term.URIRef(object)
    subject_was_fixed = False
    original_subject = subject_iri

    if subject.endswith(".-"):
        fixed_subject = subject[:-2]
        fixed_subject_iri = rdflib.term.URIRef(fixed_subject)
        graph.add((subject_iri, RDF.type, RDFS.Class))
        graph.add((subject_iri, OWL.equivalentClass, fixed_subject_iri))
        #graph.add((subject_iri, RDFS.subClassOf, fixed_subject_iri))
        subject_iri = fixed_subject_iri
        subject_was_fixed = True

    if object.endswith(".-"):
        fixed_object = object[:-2]
        fixed_object_iri = rdflib.term.URIRef(fixed_object)
        graph.add((object_iri, RDF.type, RDFS.Class))
        graph.add((object_iri, OWL.equivalentClass, fixed_object_iri))
        #graph.add((object_iri, RDFS.subClassOf, fixed_object_iri))
        object_iri = fixed_object_iri

    #create and add class + label to the graph
    graph.add((subject_iri, RDF.type, RDFS.Class))
    graph.add((subject_iri, RDFS.label, Literal(row_element, lang='de')))
    if subject_iri in fr_labels:
        graph.add((subject_iri, RDFS.label, Literal(fr_labels[subject_iri], lang='fr')))
    if object_iri in fr_labels:
        graph.add((object_iri, RDFS.label, Literal(fr_labels[object_iri], lang='fr')))
    graph.add((subject_iri, RDFS.subClassOf, object_iri))

    #if subject_was_fixed:
    #    graph.add((original_subject, RDFS.subClassOf, object_iri))

def _get_bfarm_version(year:str, bfarm_versions=BfarmVersion):
    """
    gets the bfarm version for a given sphn version

    Returns:
        int: bfarm year
    """
    _construct_enum_key = f"YEAR_{year}"
    member_list =  list(bfarm_versions._member_map_.keys())
    if _construct_enum_key in member_list:
        bfarm_year = getattr(bfarm_versions, _construct_enum_key)
        return bfarm_year.value
    else:
        return year

def _add_metadata(graph:Graph, orga_enum:Enum, orga_tuple:tuple, year:int, namespace, PAV, bfarm_year:int):

    #Define slices for tuple unpacking
    ORGA_ENUM = 0
    IRI = 1

    # create organization
    orga = rdflib.term.URIRef(namespace + f"{orga_tuple[ORGA_ENUM].name}")

    # create literals
    orga_literal = rdflib.Literal(orga_tuple[ORGA_ENUM].value)

    graph.add(triple=(orga, RDF.type, PROV.Entity))
    # check for BFARM and if encountered add the correct version
    if orga_enum == Organizations.BFARM.name:
        version = rdflib.Literal(bfarm_year, datatype=XSD.string)
        graph.add(triple=(orga, PAV.version, version))
    else:
        version = rdflib.Literal(year, datatype=XSD.string)
        graph.add(triple=(orga, PAV.version, version))

    # finally add the triples to the graph
    graph.add(triple=(orga, PAV.createdBy, orga_tuple[IRI]))
    graph.add(triple=(orga_tuple[IRI], RDF.type, PROV.Organization))
    graph.add(triple=(orga_tuple[IRI], FOAF.name, orga_literal))


def export_turtle_file(graph, CREATEVERSIONED):
    file_name = "sphn_icd-10-gm_" + YEAR + "-" + VERSION + ".ttl"
    if len(CREATEVERSIONED) > 0:
        file_name = "sphn_icd-10-gm_" + YEAR + "-" + VERSION + "_versioned.ttl"
    file = open(file_name, "w")
    file.write(graph.serialize(format="turtle").decode("utf-8"))
    file.close()

if __name__ == '__main__':
    # Path to files:
    # ICD-10-GM contains 3 files where sys.argv[3] is for the codes,
    # sys.argv[4] is for groups, and sys.argv[5] is for chapters
    CREATEVERSIONED = ""
    if len(sys.argv) == 7:
        CREATEVERSIONED = sys.argv[6]
    elif len(sys.argv) == 10:
        CREATEVERSIONED = sys.argv[9]
    codes = pd.read_csv(sys.argv[3], sep=";", header = None)
    groups = pd.read_csv(sys.argv[4], sep=";", header = None)
    chapters = pd.read_csv(sys.argv[5], sep=";", header = None)
    # Some versions of ICD-10-GM may also contain 3 additional files where sys.argv[6] is for the codes in french,
    # sys.argv[7] is for groups in french, and sys.argv[8] is for chapters in french
    fr_codes = pd.read_csv(sys.argv[6], sep=";", header = None) if len(sys.argv) > 8 else None
    fr_groups = pd.read_csv(sys.argv[7], sep=";", header = None) if len(sys.argv) > 8 else None
    fr_chapters = pd.read_csv(sys.argv[8], sep=";", header = None) if len(sys.argv) > 8 else None
    graph = create_rdf(codes, groups, chapters, CREATEVERSIONED, **{'fr_codes': fr_codes, 'fr_groups': fr_groups, 'fr_chapters': fr_chapters})
    export_turtle_file(graph, CREATEVERSIONED)
