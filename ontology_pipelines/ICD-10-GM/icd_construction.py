with open("cim10gm2012syst_ascii_20121022.txt", "r") as f:
    lines = f.readlines()

output = []
prev_line = None

for line in lines:
    if line.startswith((' ', '\t')) and prev_line is not None:
        prev_line = prev_line.rstrip() + " " + line.lstrip()
    else:
        if prev_line is not None:
            output.append(prev_line)
        prev_line = line

if prev_line is not None:
    output.append(prev_line)

with open("purged_whitelines.txt", "w") as f:
    f.writelines(output)

############################################################
# Chapter file generation

with open("purged_whitelines.txt", "r") as f:
    lines = f.readlines()

output = []
counter = 0

for line in lines:
    if line.startswith('0T'):
        line = line[2:].lstrip()
        if not (line.startswith('Chapitre') or line.startswith('(')):
            counter += 1
            # print(f"{counter:02};" + line)
            output.append(f"{counter:02};" + line)
    
with open('CIM10GM2012_ASCII_S_FR_versionmtadonne_chapitres.txt', 'w') as f:
    f.writelines(output)

#############################################################
# Chapterdict generation

with open("purged_whitelines.txt", "r") as f:
    lines = f.readlines()

counter = 0
chapters = {}

for line in lines:
    if line.startswith('0T'):
        line = line[2:].lstrip()
        if line.startswith('('):
            counter += 1
            chapters[f"{counter:02}"] = [string.replace('(', '').replace(')', '').replace('\n', '') for string in line.split('-')]
print(chapters)


################################################################

def get_key(codes):
    first, second = codes
    # print(first, second)
    for key, (item1, item2) in chapters.items():
        if second <= item2 and first >= item1:
            # print(key)
            return key
        else:
            continue


with open("purged_whitelines.txt", "r") as f:
    lines = f.readlines()

output = []

for line in lines:
    if line.startswith('0G'):
        line = line[2:].lstrip()
        if not line.startswith('Ce chapitre'):
            lines = [line[0:7], line[7:].lstrip(' ')] 
            codes = lines[0].split('-')
            key = get_key(codes)
            # print(f"{codes[0]};{codes[1]};{key};" + lines[1])
            output.append(f"{codes[0]};{codes[1]};{key};" + lines[1])
        

with open('CIM10GM2012_ASCII_S_FR_versionmtadonne_groupes.txt', 'w') as f:
    f.writelines(output)

#####################################################################

def get_single_key(code):
    for key, items in chapters.items():
        # print(items[0], items[1], code)
        # print('A00' >= 'A00')
        # print(code >= items[0], code <= items[1])
        if code >= items[0] and (code <= items[1] or items[1] in code):
            return key
        else:
            continue
    raise "Error"


with open("purged_whitelines.txt", "r") as f:
    lines = f.readlines()

output = []
prev_code = '99999999'

for line in lines:
    if line.startswith(('4T', '5T', '6T')):
        line = line[0:12].replace('+', ' ') + line[12:]
        excl = '!' in line[0:12]
        star = '*' in line[0:12]
        line = line.replace('!', ' ').replace('*', ' ')
        level, letter, rest = line[:1], line[1:2], line[2:]
        try: 
            code, rest = rest.split('      ')
        except:
            code, rest = rest.split('   ')

        
        code = code.strip()
        rest = rest.rstrip('\n').lstrip()
        # print(level, letter, code , "###", repr(rest))


        if prev_code in code:
            if '.' in prev_code:
                csv_line =  output[-1]
                csv_line = csv_line[0:12] + csv_line[12:].replace(prev_code, prev_code + '-', 1)
                output[-1] = csv_line
                print(output[-1])
            else:
                csv_line =  output[-1]
                csv_line = csv_line[0:12] + csv_line[12:].replace(prev_code, prev_code + '.-', 1)
                output[-1] = csv_line
                print(output[-1])

        prev_code = code
        exact_code = code

        
        # exact_code = code if '.' in code else code + '.-' # so essentially if there is . in the code then we have the logic that: C79.8 C79.81 exist -> C79.8- C79.81
        """
        SO: track and remove ! * etc,
        do the splitting etc, 
        for .-:
            check if . in code:
                check if last code is subset of this code then in last code replace the code with code + "-" because we have something like C79.8 C79.81 exist -> C79.8- C79.81
            else:
                add .-
        and then append tracked items

        """

        if excl:
            exact_code += '!'
        elif star:
            exact_code += '*'

        key = get_single_key(code)
        print(f"{int(level)-1};{letter};?;{key};???;{exact_code};{code};{code.replace('.','')};{rest};Base Description;Desctiption;;9;9;999;9999;999;9999;9;N;J")
        output.append(f"{int(level)-1};{letter};?;{key};???;{exact_code};{code};{code.replace('.','')};{rest};Base Description;Desctiption;;9;9;999;9999;999;9999;9;N;J\n")
        # 4;T;X;21;Z80;Z91.8;Z91.8;Z918;Antécédents personnels d'autres facteurs de risque précisés, non classés ailleurs;Antécédents personnels de facteurs de risque, non classés ailleurs;Antécédents personnels d'autres facteurs de risque précisés, non classés ailleurs;;9;9;999;9999;999;9999;9;N;J

        # 3;N;X;21;Z80;Z91.-;Z91;Z91;Antécédents personnels de facteurs de risque, non classés ailleurs;Antécédents personnels de facteurs de risque, non classés ailleurs;;;9;9;999;9999;999;9999;9;N;J

with open('CIM10GM2012_ASCII_S_FR_versionmtadonne_codes.txt', 'w') as f:
    f.writelines(output)