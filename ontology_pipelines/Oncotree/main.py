
import datetime
import sys
import json
import logging

from rdflib import Graph, URIRef, Literal
from rdflib.namespace import DC, OWL, RDF, RDFS, SKOS, XSD, DCTERMS


NAMESPACE = URIRef('https://biomedit.ch/rdf/sphn-resource/sphn/oncotree')
PREFIX_MAP = {
    'rdf': RDF,
    'rdfs': RDFS,
    'owl': OWL,
    'dc': DC,
    'dcterms': DCTERMS,
    'NCIT': URIRef('http://purl.obolibrary.org/obo/NCIT_'),
    'UMLS': URIRef('https://uts.nlm.nih.gov/uts/umls/concept/'),
}


def to_flat_dictionaries(treeJSON: dict, parentCode : str) -> list:
    """
    Flatten the tree representation to a list.
    """
    nodes = []
    for _, value in treeJSON.items():
        node={'name': value['name'], 'code': value['code']}
        if parentCode != '':
            node['parent'] = parentCode

        if not value['mainType'] is None:
            node.update({'mainType': value['mainType']})

        if not value['externalReferences'] is None:
            if 'UMLS' in value['externalReferences'] and not value['externalReferences']['UMLS'] is None:
                node.update({'UMLS': value['externalReferences']['UMLS']})
            if 'NCI' in value['externalReferences'] and not value['externalReferences']['NCI'] is None:
                node.update({'NCIT': value['externalReferences']['NCI']})

        nodes += [node]
        children = value.get('children')
        if children is None:
            pass
        else:
            nodes += to_flat_dictionaries(children,node['code'])
    return nodes

def build_graph(graph, baseUri: str, nodes: list) -> Graph:
    has_main_type = URIRef(baseUri + '#hasMainType')
    has_umls_external_reference = URIRef(baseUri + '#hasUMLSExternalReference')
    has_ncit_external_reference = URIRef(baseUri + '#hasNCITExternalReference')

    for node in nodes:
        code=node.get('code')
        if code is None:
            raise Exception('missing code to node')
        name=node.get('name')
        if name is None:
            raise Exception('missing name to node')
        parent = node.get('parent')

        uri = URIRef(f'{baseUri}#{code}')
        name = Literal(name, lang='en')
        graph.add((uri, RDF.type, OWL.Class))
        graph.add((uri, RDFS.label, name))
        if not node.get('mainType') is None:
            mainTypeRef = Literal(node.get('mainType'), lang = 'en')
            graph.add((uri, has_main_type, mainTypeRef))

        if not node.get('UMLS') is None:
            for umls in node.get('UMLS'):
                umlsRef = URIRef(PREFIX_MAP['UMLS'] + umls)
                graph.add((uri, has_umls_external_reference, umlsRef))

        if not node.get('NCIT') is None:
            for ncit in node.get('NCIT'):
                ncitRef = URIRef(PREFIX_MAP['NCIT'] + ncit)
                graph.add((uri, has_ncit_external_reference, ncitRef))

        if parent:
            parentUri = URIRef(f"{baseUri}#{parent}")
            graph.add((uri, RDFS.subClassOf, parentUri))
    return graph


def buildHeaderGraph(baseUri: URIRef, version: str) -> Graph:
    """
    Add metadata to the graph.
    """
    description = '''
    The RDF OncoTree Classification built from OncoTree API (see more on http://oncotree.mskcc.org/).

    OncoTree: A Cancer Classification System for Precision Oncology

    Kundra, Ritika and Zhang, Hongxin and Sheridan, Robert and Sirintrapun, Sahussapont Joseph and Wang, Avery and Ochoa, Angelica and Wilson, Manda and Gross, Benjamin and Sun, Yichao and Madupuri, Ramyasree and Satravada, Baby A. and Reales, Dalicia and Vakiani, Efsevia and Al-Ahmadie, Hikmat A. and Dogan, Ahmet and Arcila, Maria and Zehir, Ahmet and Maron, Steven and Berger, Michael F. and Viaplana, Cristina and Janeway, Katherine and Ducar, Matthew and Sholl, Lynette and Dogan, Snjezana and Bedard, Philippe and Surrey, Lea F. and Sanchez, Iker Huerga and Syed, Aijaz and Rema, Anoop Balakrishnan and Chakravarty, Debyani and Suehnholz, Sarah and Nissan, Moriah and Iyer, Gopakumar V. and Murali, Rajmohan and Bouvier, Nancy and Soslow, Robert A. and Hyman, David and Younes, Anas and Intlekofer, Andrew and Harding, James J. and Carvajal, Richard D. and Sabbatini, Paul J. and Abou-Alfa, Ghassan K. and Morris, Luc and Janjigian, Yelena Y. and Gallagher, Meighan M. and Soumerai, Tara A. and Mellinghoff, Ingo K. and Hakimi, Abraham A. and Fury, Matthew and Huse, Jason T. and Bagrodia, Aditya and Hameed, Meera and Thomas, Stacy and Gardos, Stuart and Cerami, Ethan and Mazor, Tali and Kumari, Priti and Raman, Pichai and Shivdasani, Priyanka and MacFarland, Suzanne and Newman, Scott and Waanders, Angela and Gao, Jianjiong and Solit, David and Schultz, Nikolaus

    DOI: 10.1200/CCI.20.00108
    '''
    hasMainType = URIRef(baseUri + '#hasMainType')
    hasExternalReference = URIRef(baseUri + '#hasExternalReference')
    hasUMLSExternalReference = URIRef(baseUri + '#hasUMLSExternalReference')
    hasNCITExternalReference = URIRef(baseUri + '#hasNCITExternalReference')

    title = 'The RDF version of the OncoTree classification'
    copyright = Literal(
        "SPHN DCC (PHI, SIB Swiss Institute of Bioinformatics) adapted model of the OncoTree classification: OncoTree is an open-source classification sytem that was developed at Memorial Sloan Kettering Cancer Center (MSK) for standardizing cancer type diagnosis from a clinical perspective by assigning each diagnosis a unique OncoTree code.")
    license = "https://creativecommons.org/licenses/by/4.0/"

    namespace_version = URIRef(f'{baseUri}/{version}')

    graph=Graph()
    graph.bind('oncotree', NAMESPACE + '#')
    for key, value in PREFIX_MAP.items():
        graph.namespace_manager.bind(key, value)

    graph.add((baseUri,RDF.type,OWL.Ontology))
    graph.add((NAMESPACE, OWL.versionIRI, namespace_version))
    graph.add((baseUri, DCTERMS.created, Literal(str(datetime.datetime.now().date()))))
    graph.add((baseUri, DCTERMS.license, URIRef(license)))
    graph.add((baseUri,DC.title,Literal(title, lang='en')))
    graph.add((baseUri,DC.description, Literal(description, lang='en')))
    graph.add((baseUri, DC.rights, copyright))

    graph.add((hasMainType, RDF.type, OWL.AnnotationProperty))
    graph.add((hasMainType, RDFS.label, Literal("has main type",lang='en')))
    graph.add((hasMainType, SKOS.definition, Literal("The tumor category that is used for cancer classification.",lang='en')))

    graph.add((hasExternalReference, RDF.type, OWL.AnnotationProperty))
    graph.add((hasExternalReference, RDFS.label, Literal("has external reference",lang='en')))
    graph.add((hasExternalReference, SKOS.definition, Literal("The reference to a term from an external terminology.",lang='en')))

    graph.add((hasUMLSExternalReference, RDF.type, OWL.AnnotationProperty))
    graph.add((hasUMLSExternalReference, RDFS.label, Literal("has main type",lang='en')))
    graph.add((hasUMLSExternalReference, SKOS.definition, Literal("The reference to a code from the Unified Medical Langauge System (UMLS).",lang='en')))
    graph.add((hasUMLSExternalReference, RDFS.subPropertyOf, hasExternalReference))

    graph.add((hasNCITExternalReference, RDF.type, OWL.AnnotationProperty))
    graph.add((hasNCITExternalReference, RDFS.label, Literal("has main type",lang='en')))
    graph.add((hasNCITExternalReference, SKOS.definition, Literal("The reference to a code from the National Cancer Institute Thesaurus (NCIt).",lang='en')))
    graph.add((hasNCITExternalReference, RDFS.subPropertyOf, hasExternalReference))
    return graph



def main():
    filename = sys.argv[1]
    output_filename = sys.argv[2]
    version = sys.argv[3]

    oncotree_data = json.load(open(filename))
    flatten_nodes = to_flat_dictionaries(oncotree_data, '')
    graph = buildHeaderGraph(NAMESPACE, version)
    graph = build_graph(graph, NAMESPACE, flatten_nodes)
    graph.serialize(destination=output_filename, format='turtle')
    logging.info(f'successfully written ontology file to {output_filename}')


if __name__ == '__main__':
    main()