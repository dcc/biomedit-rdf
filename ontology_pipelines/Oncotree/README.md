# OncoTree Ontology Pipeline

The pipeline is for the conversion of OncoTree classification to its RDF representation.

OncoTree can be downloaded as JSON from https://oncotree.mskcc.org/api/tumorTypes/tree

The API call should default to the latest stable release of OncoTree (2021-11-02).

The resulting JSON is the input for the pipeline:

```sh
python main.py oncotree_data.json oncotree.ttl 2021-11-02
```
