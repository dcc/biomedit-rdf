# SNOMED CT Ontology in RDF

SNOMED CT is converted in RDF using the [SNOMED Owl Toolkit](https://github.com/IHTSDO/snomed-owl-toolkit) and a home-made script.

Only the January and the July releases are translated into RDF in SPHN.

## Copyright statement
The copyright follows the instructions provided by SNOMED CT (https://www.snomed.org/), SNOMED CT is copyright © SNOMED International 2021 v3.15.1., SNOMED CT international. In order to use the file please register with eHealth Suisse for an affiliate license for SNOMED CT (free of charge) https://mlds.ihtsdotools.org/#/landing/CH?lang=en.