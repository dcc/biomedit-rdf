package swiss.sib.rdf.tools;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.tdb2.TDB2Factory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.query.Dataset;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.Lang;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.query.ARQ;
import org.apache.jena.vocabulary.OWL2;

import org.apache.commons.cli.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;
import java.util.Set;
import java.util.List;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;

public class triples_merger {
    static Options options = null;
    static CommandLineParser parser = null;

    public static void createParser(){
        // create the command line parser
        parser = new DefaultParser();

        // create the Options
        options = new Options();
        options.addOption( "i", "input", true, "input file path" );
        options.addOption( "o", "output", true, "output file path" );
        options.addOption( "p", "prefixes", true, "prefixes file path" );
        options.addOption( "d", "dateOfVersion", true, "version date to be overwritten");
        options.addOption("h", "help", false, "prints this help");

    }

    public static void main( String[] args ) {
        ArrayList<String> inputfilenames = new ArrayList<String>();
        String outputfilename = "";
        String prefixfilename = "";
        String dateOfVersion = "";

        createParser();
        try {
            // parse the command line arguments
            CommandLine line = parser.parse( options, args );
            if (line.getOptionValue("i") == null || line.getOptionValue("o") == null || line.hasOption("h")) {
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp( "swiss.sib.rdf.tools.triples_merger", options );
                System.exit(0);
            }
            inputfilenames = new ArrayList<>(Arrays.asList(line.getOptionValues("i")));
            outputfilename = line.getOptionValue("o");
            prefixfilename = line.getOptionValue("p");
            dateOfVersion = line.getOptionValue("d");
        }
        catch (ParseException exp) {
            System.err.println( "Parsing failed.  Reason: " + exp.getMessage() );
        }

        Model model = ModelFactory.createDefaultModel() ;
        ARQ.init();
        for (String inputfilename : inputfilenames) {
            try {
                System.out.println("Current model size is: " + Long.toString(model.size()));
                System.out.println("Loading from file " + inputfilename);
                model.read(new FileInputStream(inputfilename), null, "TURTLE");
            }
            catch (Exception ex) {
                System.err.println("Loading the ontology from "+ inputfilename + " failed. " + ex.getMessage());
            }
        }
        System.out.println("Model size after merging is: " + Long.toString(model.size()));

        if (prefixfilename != null && model != null) {
            try (InputStream prefixStream = new FileInputStream(prefixfilename)) {
                Properties prop = new Properties();
                prop.load(prefixStream);
                Set<Object> prefixes = prop.keySet();
                for(Object prefix:prefixes){
                    String prefixString = (String)prefix;
                    System.out.println("Applying prefix " + prefixString +" : " + prop.getProperty(prefixString));
                    model.setNsPrefix( prefixString, prop.getProperty(prefixString) );
                    }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (dateOfVersion != null && model != null) {
            org.apache.jena.rdf.model.Resource targetDate = model.createResource("http://snomed.info/sct/900000000000207008/version/" + dateOfVersion);
            List<Resource> resources = model.listResourcesWithProperty(OWL2.versionIRI).toList();
            for (org.apache.jena.rdf.model.Resource res : resources) {
                Statement s = model.getProperty(res, OWL2.versionIRI);
                model.remove(s);
                res.addProperty(OWL2.versionIRI, targetDate);

            }

        }
        
        if (model != null) {
            try {
                OutputStream outfile = new FileOutputStream(outputfilename, false);
                RDFDataMgr.write(outfile, model, Lang.TURTLE);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }


    }
}
