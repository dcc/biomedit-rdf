package swiss.sib.rdf.tools;

import org.apache.commons.cli.*;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.formats.TurtleDocumentFormat;
import org.semanticweb.owlapi.model.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

public class owl_converter {
    static Options options = null;
    static CommandLineParser parser = null;

    public static void createParser(){
        // create the command line parser
        parser = new DefaultParser();

        // create the Options
        options = new Options();
        options.addOption( "i", "input", true, "input file path" );
        options.addOption( "o", "output", true, "output file path" );
        options.addOption("h", "help", false, "prints this help");

    }

    public static void main( String[] args ) {
        OWLDocumentFormat outputFormat = new TurtleDocumentFormat();
        //OWLOntologyFormat outputFormat = new TurtleOntologyFormat();
        String inputfilename = "";
        String outputfilename = "";

        createParser();
        try {
            // parse the command line arguments
            CommandLine line = parser.parse( options, args );
            if (line.getOptionValue("i") == null || line.getOptionValue("o") == null || line.hasOption("h")) {
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp( "swiss.sib.rdf.tools.owl_converter", options );
                System.exit(0);
            }
            inputfilename = line.getOptionValue("i");
            outputfilename = line.getOptionValue("o");
        }
        catch( ParseException exp ) {
            // oops, something went wrong
            System.err.println( "Parsing failed.  Reason: " + exp.getMessage() );
        }

        final OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
        OWLOntology ontology = null;
        try {
            ontology = manager.loadOntologyFromOntologyDocument(new File(inputfilename));
        }
        catch (OWLOntologyCreationException owlex) {
            System.err.println("Loading the ontology failed. " + owlex.getMessage());
        }
        if (ontology != null) {
            try {
                OutputStream outfile = new FileOutputStream(outputfilename, false);
                manager.saveOntology(ontology, outputFormat, outfile);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (OWLOntologyStorageException e) {
                e.printStackTrace();
            }

        }


    }
}
