# SO Ontology Pipeline

This pipeline is just to convert OWL XML representation of Sequence Ontology (SO) to an RDF Turtle representation.

The SO OWL can be downloaded from http://purl.obolibrary.org/obo/so.owl.

## Copyright statement

SO is maintained by the Eilbeck Lab, Department of Biomedical Informatics, University of Utah, Salt Lake City.
SO data and data products are licensed under the Creative Commons Attribution 4.0 Unported License (http://www.sequenceontology.org/?page_id=345).
