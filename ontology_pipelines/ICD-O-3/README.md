# ICD-O-3 Ontology Pipeline

The pipeline is for the conversion of ICD-O-3 classification to its RDF representation.

More specifically, this pipeline is for the conversion of ICD-O-3 fetched
from the European Commission Joint Research Data Catalogue: https://data.jrc.ec.europa.eu/dataset/88ff4ec5-1832-403e-abe1-64928592568f


## Pre-processing

First, download the `ICDO3_O.zip` from https://data.jrc.ec.europa.eu/dataset/88ff4ec5-1832-403e-abe1-64928592568f

Then load all the OWL files in the archive into Protégé. Finally, merge the ontologies to yield a single OWL representation that is a union of all the OWL files from `ICDO3_O.zip`.

The resulting meregd OWL file is then the input to the pipeline:

```sh
python main.py ICDO3_v2_0.owl icd-o-3-v2_0.ttl
```
