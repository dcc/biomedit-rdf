import sys
from typing import Dict
from rdflib import OWL, RDF, RDFS, Graph



PREFIX_MAP = {
    'ICD-O-3': 'https://data.jrc.ec.europa.eu/collection/ICDO3_O#',
    'NCIT': 'http://purl.obolibrary.org/obo/NCIT_',
}


def prepare_rdf_graph(graph: Graph, prefix_map: Dict) -> Graph:
    """
    Prepare an instance of ``rdflib.Graph`` with the required metadata.

    Args:
        prefix_map: A dictionary containing prefix to IRI mappings

    Returns:
        An instance of ``rdflib.Graph``

    """
    normalize_prefixes(graph, prefix_map)
    add_labels(graph)
    return graph


def normalize_prefixes(graph: Graph, prefix_map: Dict = None) -> None:
    """
    Normalize prefix to IRI bindings as specified in ``prefix_map``.

    Args:
        graph: An instance of rdflib.Graph
        prefix_map: A dictionary of prefix to IRI mappings

    """
    if prefix_map:
        for prefix, iri in prefix_map.items():
            graph.bind(prefix, iri, override=True)


def add_labels(graph: Graph) -> None:
    """
    Replace rdfs:comment with rdfs:label for all resources.

    Args:
        graph: An instance of rdflib.Graph

    """
    for subject, object in graph.subject_objects(predicate=RDFS.comment):
        graph.remove((subject, RDFS.comment, object))
        graph.add((subject, RDFS.label, object))


if __name__ == '__main__':
    graph = Graph()
    graph.parse(sys.argv[1])
    graph = prepare_rdf_graph(graph=graph, prefix_map=PREFIX_MAP)
    graph.serialize(sys.argv[2], format='ttl')
