# import
import pandas as pd
import os
import re


it = pd.read_csv('./italian2014.csv', encoding="iso-8859-1", delimiter=';')
de = pd.read_csv('./german2014.csv', encoding="iso-8859-1", delimiter=';')
fr = pd.read_csv('./french2014.csv', encoding="iso-8859-1", delimiter=';')

def special_insert(df1, df2, name):
    df2[name] = df2['text']
    del df2['text']

    # Get columns from the old dataframe and add a new column
    columns_new = df1.columns.tolist() + [name]

    # Create an empty dataframe with the new columns
    df_new = pd.DataFrame(columns=columns_new)

    offset = 0
    # for i in range(20, 1500):
    for i in range(max(df1.shape[0], df2.shape[0])):
        code1, code2 = df1.loc[i + min(offset, 0)]['zcode'], df2.loc[i - max(offset, 0)]['zcode']
        if code1 > code2:
            offset -= 1
        elif code1 < code2:
            offset += 1
        # Insert the augmented row into df_new
        # row_data = df1.loc[i + min(offset, 0)]._append(pd.Series({name: df2.loc[i - max(offset, 0), name]}))
        df_new.loc[i, df1.columns] = df1.loc[i + min(offset, 0), df1.columns]
        df_new.loc[i, name] = df2.loc[i - max(offset, 0), name]
        print(i)
    return df_new




name = 'chopp2014.csv'
de = special_insert(de, it, 'IT')
df_out = special_insert(de, fr, 'FR')

df_out['DE'] = df_out['text']
columns_to_drop = ['emitter', 'status', 'modification date', 'database id', 'text']

# Drop the columns only if they exist in the DataFrame
df_out = df_out.drop(columns=[col for col in columns_to_drop if col in df_out.columns])

df_out.to_csv(name, sep=';', encoding="utf-8")

# import subprocess
# subprocess.run('python main.py ./' + name + ' 2014 testrun')

# print(df1.shape[0], df2.shape[0])




# # making an index but this is only semi relevant at the moment
# def create_index(df):
#     # Combine 'zcode' and 'item type' columns
#     df['index'] = df['zcode'] + '&' + df['item type']

#     # Function to extract the last bracketed content
#     def extract_last_bracketed(text):
#         matches = re.findall(r'\(([^)]+)\)', text)  # Find all bracketed content
#         return matches[-1] if matches else ''  # Return the last one or an empty string if no matches

#     # Extract the last bracketed content from 'text' column and add it to 'combined' column
#     try:
#         df['index'] += '&' + df['text'].apply(extract_last_bracketed)
#     except:
#         raise "Error"
#         df['index'] += '&' + df['DE'].apply(extract_last_bracketed)
#     print(df)
    
# for df in [de, fr, it]:
#     create_index(df)
#     df.set_index('index', inplace=True)

# duplicates = de[de.index.duplicated(keep=False)]
# index_counts = duplicates.index.value_counts()

# de['DE'] = de['text']
# del de['text']

# for df in [it, fr]:
#     result = de.join(df[['text']], how='left')
#     result.rename(columns={'text': 'IT'}, inplace=True)
#     de = result

# de = de.reset_index()
# de.to_csv('output.csv', sep=';')