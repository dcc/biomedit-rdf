# This Python script transforms the CHOP classification into an RDF.

import pandas as pd
import sys
import rdflib

from rdflib import Graph, Literal
from rdflib.namespace import RDFS, RDF, OWL

# namespace of CHOP defined within SPHN:
# 1) year MUST be changed according to the release year of CHOP
# 1) version MUST be changed with every change in this code affecting the RDF schema.
YEAR = sys.argv[2]
VERSION = sys.argv[3]
CHOP_NAMESPACE = "https://biomedit.ch/rdf/sphn-resource/chop/"
NAMESPACE_VERSION = "https://biomedit.ch/rdf/sphn-resource/chop/" + YEAR + "/" + VERSION

# Dict to keep track of the 'current' hierarchy
level_parent = {"1": "", "2": "", "3": "", "4": "", "5": ""}

def import_excel(file_name):
    df = pd.read_csv(file_name, sep=";")
    return df

def create_rdf(data, version):
    # create RDF graph:
    graph = Graph()

    # Header:
    graph.bind("rdfs", RDFS)
    graph.bind("rdf", RDF)
    if len(version) > 0:
        NAMESPACE = "https://biomedit.ch/rdf/sphn-resource/chop/" + str(version) + "/"
    else:
        NAMESPACE = "https://biomedit.ch/rdf/sphn-resource/chop/"
    graph.bind("chop", NAMESPACE)
    graph.bind("owl", OWL)

    # set version
    sphn_chop = rdflib.term.URIRef(CHOP_NAMESPACE)
    sphn_chop_version = rdflib.term.URIRef(NAMESPACE_VERSION)
    graph.add((sphn_chop, OWL.versionIRI, sphn_chop_version))
    graph.add((sphn_chop, RDF.type, OWL.Ontology))

    # add copyright information
    copyright = Literal(
        "RDF version of CHOP (https://www.bfs.admin.ch/bfs/de/home/statistiken/gesundheit/nomenklaturen/medkk/instrumente-medizinische-kodierung.html), developed by the SPHN DCC (PHI, SIB Swiss Institute of Bioinformatics). "
        "The copyright follows instructions by the Federal Statistical Office (FSO), Neuchâtel " + YEAR + ": “Wiedergabe unter Angabe der Quelle für nichtkommerzielle Nutzung gestattet” (i.e., Reproduction is authorized, except for commercial purposes, if the source is acknowledged).")
    graph.add((sphn_chop, RDFS.comment, copyright))


    root_node = rdflib.term.URIRef(CHOP_NAMESPACE + "CHOP")
    graph.add((root_node, RDF.type, RDFS.Class))
    graph.add((root_node, RDFS.label, Literal("CHOP")))


    # parse Excel file to extract classes and hierarchies:
    for index, row in data.iterrows():
        # T stands for title and indicates the row corresponding to the definition of the code.
        if row['item type'] == "T":
            if row['zcode'].startswith("C"):
                # First level differs because of the Chapter ID
                subject = rdflib.term.URIRef(NAMESPACE + row['zcode'])
                graph.add((subject, RDF.type, RDFS.Class))
                # Adding to the RDF the label in FR, DE and IT
                set_label(row, subject, graph)
                graph.add((subject, RDFS.subClassOf, root_node))
                level_parent["1"] = subject
            else:
                set_class(row, graph, level_parent, NAMESPACE)
    return graph

def set_class(row, g, level_parent, NAMESPACE):
    level = row['indent level']
    subject = rdflib.term.URIRef(NAMESPACE + row['zcode'][1:])
    g.add((subject, RDF.type, RDFS.Class))

    if level == 2:
        level_parent["2"] = subject
        object = level_parent["1"]
    elif level == 3:
        # Parent is Z--
        level_parent["3"] = subject
        object = level_parent["2"]
    elif level == 4:
        # Parent is Z--.-
        level_parent["4"] = subject
        object = level_parent["3"]
    elif level == 5:
        # Parent is Z--.--
        level_parent["5"] = subject
        object = level_parent["4"]
    elif level == 6:
        # Parent is Z--.--.-
        object = rdflib.term.URIRef(level_parent["5"])

    g.add((subject, RDFS.subClassOf, object))
    set_label(row, subject, g)

def export_turtle_file(graph, CREATEVERSIONED):
    file_name = "sphn_chop_" + YEAR + "-" + VERSION + ".ttl"
    if len(CREATEVERSIONED) > 0:
        file_name = "sphn_chop_" + YEAR + "-" + VERSION + "_versioned.ttl"
    file = open(file_name, "w")
    file.write(graph.serialize(format="turtle").decode("utf-8"))
    file.close()

# Label to be set in three languages: DE, FR, IT
def set_label(row, subject, g):
    if row["DE"]:
        object = Literal(row["DE"], lang="de")
        g.add((subject, RDFS.label, object))
    if row["FR"]:
        object = Literal(row["FR"], lang="fr")
        g.add((subject, RDFS.label, object))
    if row["IT"]:
        object = Literal(row["IT"], lang="it")
        g.add((subject, RDFS.label, object))

if __name__ == '__main__':
    # path to file must be given as argument #1
    filePath = sys.argv[1]
    CREATEVERSIONED=""
    if len(sys.argv) == 5: 
        CREATEVERSIONED = sys.argv[4]
    data = import_excel(filePath)
    graph = create_rdf(data, CREATEVERSIONED)
    export_turtle_file(graph, CREATEVERSIONED)
