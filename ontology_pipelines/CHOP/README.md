# CHOP classification in RDF

Contains the script to transform CHOP classification into RDF by SPHN.

## Copyright statement
The copyright of CHOP follows the instructions provided by FSO, Neuchâtel 2022: “Wiedergabe unter Angabe der Quelle für nichtkommerzielle Nutzung gestattet” (i.e., Reproduction is authorized, except for commercial purposes, if the source is acknowledged).