# Python script: translate EMDN into RDF
import pandas as pd
import sys

import rdflib
from rdflib import Graph, RDF, RDFS, Literal, OWL

YEAR = sys.argv[2]
# version of the EMDN RDF given as argument #3
VERSION = sys.argv[3]

# Define namespaces
NAMESPACE = "https://biomedit.ch/rdf/sphn-resource/emdn/"
NAMESPACE_VERSION = "https://biomedit.ch/rdf/sphn-resource/emdn/" + YEAR + "/" + VERSION

# Dict to keep track of the 'current' hierarchy
level_parent = {"0": "", "1.1": "", "2": "", "3": "", "4": "", "5": "", "6": "", "7": ""}

def import_excel(file_name):
    df = pd.read_excel(file_name)
    return df

def create_rdf(data):
    # create RDF graph:
    graph = Graph()

    # header:
    graph.bind("rdfs", RDFS)
    graph.bind("rdf", RDF)
    graph.bind("emdn", NAMESPACE)
    graph.bind("owl", OWL)

    # set version
    emdn = rdflib.term.URIRef(NAMESPACE)
    emdn_version = rdflib.term.URIRef(NAMESPACE_VERSION)
    graph.add((emdn, OWL.versionIRI, emdn_version))
    graph.add((emdn, RDF.type, OWL.Ontology))

    # add copyright information
    copyright = Literal(
        "RDF version of EMDN (https://webgate.ec.europa.eu/dyna2/emdn/), developed by the SPHN DCC (PHI, SIB Swiss Institute of Bioinformatics)."
        "The copyright follows instructions from the European Commission, 1995-2021: 'content owned by the EU is licensed under the Creative Commons Attribution 4.0 International (CC BY 4.0) licence. This means that reuse is allowed, provided appropriate credit is given and changes are indicated.'")
    graph.add((emdn, RDFS.comment, copyright))

    # set root node (EMDN) to which all codes will be subclasses
    root_node = rdflib.term.URIRef(NAMESPACE + "EMDN")
    graph.add((root_node, RDF.type, RDFS.Class))
    graph.add((root_node, RDFS.label, Literal("EMDN")))

    level_parent["0"] = root_node

    # parse Excel file to extract classes and hierarchies:
    data.fillna("nan", inplace=True)
    for index, row in data.iterrows():
        if row['EMDN CODE'] != 'nan':
            set_classes(row, graph, level_parent)
    return graph

def set_classes(row, g, level_parent):
    level = row['LEVEL ']
    subject = rdflib.term.URIRef(NAMESPACE + row['EMDN CODE'])
    g.add((subject, RDF.type, RDFS.Class))
    g.add((subject, RDFS.label, Literal(row["EMDN EN DESCRIPTION_29092021"], lang="en")))

    if(level == 1):
        level_parent["1"] = subject
        object = level_parent["0"]
    elif level == 2:
        level_parent["2"] = subject
        object = level_parent["1"]
    elif level == 3:
        level_parent["3"] = subject
        object = level_parent["2"]
    elif level == 4:
        level_parent["4"] = subject
        object = level_parent["3"]
    elif level == 5:
        level_parent["5"] = subject
        object = level_parent["4"]
    elif level == 6:
        level_parent["6"] = subject
        object = level_parent["5"]
    elif level == 7:
        object = level_parent["6"]

    g.add((subject, RDFS.subClassOf, object))

def export_turtle_file(graph):
    file_name = "sphn_emdn_" + YEAR + "-" + VERSION + ".ttl"
    file = open(file_name, "w")
    file.write(graph.serialize(format="turtle").decode("utf-8"))
    file.close()

if __name__ == '__main__':
    # path to file must be given as argument #1
    filePath = sys.argv[1]
    data = import_excel(filePath)
    graph = create_rdf(data)
    export_turtle_file(graph)
