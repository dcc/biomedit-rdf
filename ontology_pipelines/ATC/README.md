# ATC classification in RDF

Contains the script to transform ATC classification into RDF by SPHN.

## Copyright statement
ATC is provided by WHO Collaborating Centre for Drug Statistics Methodology, Oslo, Norway.

The copyright for the ATC RDF data file follows recommendations from WHO: 
"Use of all or parts of the material requires reference to the WHO Collaborating Centre for Drug Statistics Methodology. Copying and distribution for commercial purposes is not allowed. Changing or manipulating the material is not allowed."
