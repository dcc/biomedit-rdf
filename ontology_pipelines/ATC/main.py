# Python script: translate ATC into RDF
import pandas as pd
import sys
import re

import rdflib
from rdflib import Graph, RDF, RDFS, Literal, OWL

# year of release of ATC given as argument #2
YEAR = sys.argv[2]
# version of the ATC RDF given as argument #3
VERSION = sys.argv[3]

# If want to create a versioned ontology we provide argument #4

# Namespace corresponds to dereferenceable links provided by WHO
# NAMESPACE = "https://www.whocc.no/atc_ddd_index/?code="
SPHN_NAMESPACE = "https://biomedit.ch/rdf/sphn-resource/atc/"
NAMESPACE_VERSION = "https://biomedit.ch/rdf/sphn-resource/atc/" + YEAR + "/" + VERSION

# Some excels will have the column named "ATC level name", others will have it named "Substance name"
atc_substance_name = "ATC level name"

def import_excel(file_name):
    global atc_substance_name
    df = pd.read_excel(file_name)
    if df.columns.values.tolist()[1] != atc_substance_name:
        atc_substance_name = "Substance name"
    return df

def create_rdf(data, version):
    global atc_substance_name
    # create RDF graph:
    graph = Graph()
    # header:
    graph.bind("rdfs", RDFS)
    graph.bind("rdf", RDF)
    if len(version) > 0:
        NAMESPACE = "https://www.whocc.no/atc_ddd_index/?year=" + str(version) + "&code="
    else:
        NAMESPACE = "https://www.whocc.no/atc_ddd_index/?code="
    graph.bind("atc", NAMESPACE)
    graph.bind("owl", OWL)
    graph.bind("sphn-atc", SPHN_NAMESPACE)

    # set version
    sphn_atc = rdflib.term.URIRef(SPHN_NAMESPACE)
    sphn_atc_version = rdflib.term.URIRef(NAMESPACE_VERSION)
    graph.add((sphn_atc, OWL.versionIRI, sphn_atc_version))
    graph.add((sphn_atc, RDF.type, OWL.Ontology))

    # add copyright information
    copyright = Literal(
        "RDF version of ATC (https://www.whocc.no/atc_ddd_index/), developed by the SPHN DCC (PHI, SIB Swiss Institute of Bioinformatics). "
        "The copyright follows instructions by the WHO Collaborating Centre for Drug Statistics Methodology (https://www.whocc.no/copyright_disclaimer/).")
    graph.add((sphn_atc, RDFS.comment, copyright))

    # set root node (ATC) to which all codes will be subclasses
    root_node = rdflib.term.URIRef(SPHN_NAMESPACE + "ATC")
    graph.add((root_node, RDF.type, RDFS.Class))
    graph.add((root_node, RDFS.label, Literal("ATC")))

    # parse ATC classification row by row
    for index, row in data.iterrows():
        # split code to find levels
        code_split = re.split('(\d+)',row['ATC code'].strip())
        if len(code_split) == 1:
            subject = set_new_class(row, graph, NAMESPACE)
            graph.add((subject, RDFS.subClassOf, root_node))
        elif len(code_split) == 3:
            if code_split[2] == "":
                subject = set_new_class(row, graph, NAMESPACE)
                object = rdflib.term.URIRef(NAMESPACE + code_split[0])
                graph.add((subject, RDFS.subClassOf, object))
            elif len(code_split[2]) == 1:
                subject = set_new_class(row, graph, NAMESPACE)
                object = rdflib.term.URIRef(NAMESPACE + code_split[0]+code_split[1])
                graph.add((subject, RDFS.subClassOf, object))
            elif len(code_split[2]) == 2:
                subject = set_new_class(row, graph, NAMESPACE)
                object = rdflib.term.URIRef(NAMESPACE + code_split[0] + code_split[1] + code_split[2][0])
                graph.add((subject, RDFS.subClassOf, object))
        elif len(code_split) == 5:
            subject = rdflib.term.URIRef(NAMESPACE + row['ATC code'].strip())
            graph.add((subject, RDF.type, RDFS.Class))
            graph.add((subject, RDFS.label, Literal(row[atc_substance_name].strip(), lang="en")))
            object = rdflib.term.URIRef(NAMESPACE + code_split[0] + code_split[1] + code_split[2])
            graph.add((subject, RDFS.subClassOf, object))

    return graph


def set_new_class(row, graph, NAMESPACE):
    global atc_substance_name
    subject = rdflib.term.URIRef(NAMESPACE + row['ATC code'].strip())
    graph.add((subject, RDF.type, RDFS.Class))
    graph.add((subject, RDFS.label, Literal(row[atc_substance_name].strip(), lang="en")))
    return subject


def export_turtle_file(graph, CREATEVERSIONED):
    file_name = "sphn_atc_" + YEAR + "-" + VERSION + ".ttl"
    if len(CREATEVERSIONED) > 0:
        file_name = "sphn_atc_" + YEAR + "-" + VERSION + "_versioned.ttl"
    file = open(file_name, "w")
    file.write(graph.serialize(format="turtle").decode("utf-8"))
    file.close()

if __name__ == '__main__':
    # path to file must be given as argument #1
    filePath = sys.argv[1]
    CREATEVERSIONED=""
    if len(sys.argv) == 5: 
        CREATEVERSIONED = sys.argv[4]
    data = import_excel(filePath)
    graph = create_rdf(data, CREATEVERSIONED)
    export_turtle_file(graph, CREATEVERSIONED)
