import sys
from typing import Dict
from rdflib import Graph, RDF, RDFS, SKOS, DC, OWL, URIRef, Literal


PREFIX_MAP = {
    'owl': OWL,
    'OBO': 'http://purl.obolibrary.org/obo#',
    'BFO': 'http://purl.obolibrary.org/obo/BFO_',
    'ECO': 'http://purl.obolibrary.org/obo/ECO_',
    'Orphanet': 'http://www.orpha.net/ORDO/Orphanet_'
}

ORDO_main_classes = [URIRef("http://www.orpha.net/ORDO/Orphanet_C005"),
                        URIRef("http://www.orpha.net/ORDO/Orphanet_C023"),
                        URIRef("http://www.orpha.net/ORDO/Orphanet_C009"),
                        URIRef("http://www.orpha.net/ORDO/Orphanet_C003"),
                        URIRef("http://www.orpha.net/ORDO/Orphanet_C001"),
                        URIRef("http://www.orpha.net/ORDO/Orphanet_C010"),
                        URIRef("http://www.orpha.net/ORDO/Orphanet_C041")]


def prepare_rdf_graph(graph: Graph, prefix_map: Dict, version: str) -> Graph:
    """
    Prepare an instance of ``rdflib.Graph`` with the required metadata.

    Args:
        prefix_map: A dictionary containing prefix to IRI mappings
        version: The version of the ontology

    Returns:
        An instance of ``rdflib.Graph``

    """
    normalize_prefixes(graph, prefix_map)
    return graph


def normalize_prefixes(graph: Graph, prefix_map: Dict = None) -> None:
    """
    Normalize prefix to IRI bindings as specified in ``prefix_map``.

    Args:
        graph: An instance of rdflib.Graph
        prefix_map: A dictionary of prefix to IRI mappings

    """
    if prefix_map:
        for prefix, iri in prefix_map.items():
            graph.bind(prefix, iri, override=True)

def remove_original_geno_header(graph: Graph, prefix_map: Dict, version: str):
    for prefix, iri in prefix_map.items():
        graph.bind(prefix, iri)
    namespace = URIRef("https://biomedit.ch/rdf/sphn-resource/ordo/")
    namespace_version = URIRef(f"https://biomedit.ch/rdf/sphn-resource/ordo/{version}")

    copyright = Literal(
        "SPHN DCC (PHI, SIB Swiss Institute of Bioinformatics) adapted model of the ORDO OWL model: The Orphanet Rare Disease ontology (ORDO) is jointly developed by Orphanet and the EBI to provide a structured vocabulary for rare diseases capturing relationships between diseases, genes and other relevant features which will form a useful resource for the computational analysis of rare diseases. It derived from the Orphanet database (www.orpha.net ) , a multilingual database dedicated to rare diseases populated from literature and validated by international experts. It integrates a nosology (classification of rare diseases), relationships (gene-disease relations, epiemological data) and connections with other terminologies (MeSH, UMLS, MedDRA),databases (OMIM, UniProtKB, HGNC, ensembl, Reactome, IUPHAR, Geantlas) or classifications (ICD10)."    )
    graph.add((namespace, DC.rights, copyright))
    graph.add((namespace, OWL.versionIRI, namespace_version))
    graph.add((namespace, RDF.type, OWL.Ontology))
    graph.add((namespace, DC.license, URIRef("https://creativecommons.org/licenses/by/4.0/")))

    graph.remove((URIRef("http://www.orpha.net/ontology/ORDO_en_4.1.owl"), None, None))
    return graph

if __name__ == '__main__':
    """
    Automatically converts the graph into ttl format and adds versioning information

    1. input file
    2. output file
    3. file version
    """
    graph = Graph()
    graph.parse(sys.argv[1])
    graph = prepare_rdf_graph(graph=graph, prefix_map=PREFIX_MAP, version=sys.argv[3])
    # remove_original_geno_header(graph)
    graph.serialize(sys.argv[2], format='ttl')

